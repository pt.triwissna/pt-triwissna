<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\MaintenanceActiveController;
use App\Http\Controllers\MaintenanceHistoryController;
use App\Http\Controllers\DataUnitController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\MessagePerbaikanController;
use App\Http\Controllers\OperationActiveController;
use App\Http\Controllers\OperationsHistoryControllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//route Auth
Route::get('/login', [Controller::class, 'loginShow'])->name('login');
Route::post('/login/aksi', [Controller::class, 'loginAction'])->name('login.action');


Route::middleware(['auth'])->group(function () {

    Route::get('/get-realtime-data', [HomeController::class, 'getRealtimeData']);
    Route::get('/', [HomeController::class, 'index'])->name('home');

    // Route Peminjaman
    Route::get('/cetak-pinjaman-pdf/{id}', [OperationActiveController::class, 'generatePDF'])->name('cetakPinjaman.pdf');
    Route::get('/cetak-perbaikan-pdf/{id}', [MaintenanceHistoryController::class, 'generatePDF'])->name('cetakPerbaikan.pdf');

    Route::get('peminjaman', [OperationActiveController::class, 'index'])->name(('peminjaman-aktif'));
    Route::get('peminjaman/tambah/data', [OperationActiveController::class, 'tambah_data'])->name(('peminjaman-tambah-data'));
    Route::post('peminjaman/tambah/data', [OperationActiveController::class, 'insert_data'])->name(('store'));
    Route::post('peminjaman/tambah/data-selesai/{id_operation_active}', [OperationActiveController::class, 'insert_peminjaman_selesai'])->name(('in-pinjam-selesai'));
    Route::post('peminjaman/pindahkan-data', [OperationActiveController::class, 'move_data'])->name(('move-data'));
    Route::get('riwayat-peminjaman', [OperationsHistoryControllers::class, 'index'])->name('riwayat-peminjaman');
    Route::get('pesan-perubahan-peminjaman' , [MessageController::class, 'pesanOperations'])->name('pesan-perubahan-peminjaman');
    Route::put('/edit-operation/update/{id}/{id_message}', [MessageController::class, 'update_pesanPeminjaman'])->name('update_pesanPeminjaman');
    Route::get('/riwayat-perubahan-peminjaman', [MessageController::class, 'riwayatPeminjaman'])->name('riwayatPerubahanPeminjaman');
    Route::get('/riwayat-peminjaman-json', [MessageController::class, 'riwayatPeminjamanJson']);
    Route::post('pesan/operations{id}', [MessageController::class, 'store_pesan_operations'])->name('store_pesan_operations');
    Route::get('/get-operations-messages', [MessageController::class, 'getOperationsMessages']);
    Route::get('/edit-operation/show/{id}', [MessageController::class, 'edit'])->name('editOperationShow');
    // end Peminjaman

    // Perbaikan-aktif
    Route::get('perbaikan-aktif', [MaintenanceActiveController::class, 'index'])->name('perbaikan-aktif');
    Route::get('perbaikan-aktif/add', [MaintenanceActiveController::class, 'add_data'])->name('add-perbaikan-aktif');
    Route::post('perbaikan-aktif/add', [MaintenanceActiveController::class, 'insert_data'])->name('insert_maintenance');
    Route::post('perbaikan-aktif/selesai/{maintenance_active_id}', [MaintenanceActiveController::class, 'selesai_perbaikan'])->name('selesai-perbaikan');
    Route::get('riwayat-perbaikan', [MaintenanceHistoryController::class, 'index'])->name('riwayat-perbaikan');

    Route::get('pesan-perubahan-perbaikan' , [MessagePerbaikanController::class, 'pesanMaintenance'])->name('pesan-perubahan-perbaikan');
    Route::put('/edit-maintenance/update/{id}/{id_message}', [MessagePerbaikanController::class, 'update_pesanPerbaikan'])->name('update_pesanPerbaikan');
    Route::get('/riwayat-perubahan-perbaikan', [MessagePerbaikanController::class, 'riwayatPerbaikan'])->name('riwayatPerubahanPerbaikan');
    Route::get('/riwayat-perbaikan-json', [MessagePerbaikanController::class, 'riwayatPerbaikanJson']);
    Route::post('pesan/maintenance{id}', [MessagePerbaikanController::class, 'store_pesan_maintenace'])->name('store_pesan_maintenace');
    Route::get('/get-maintenance-messages', [MessagePerbaikanController::class, 'getMaintenanceMessages']);
    Route::get('/edit-maintenance/show/{id}', [MessagePerbaikanController::class, 'editmaintenance'])->name('edit-maintenance');

    // end Perbaikan-aktif

    // Route data unit
    Route::get('data-unit', [DataUnitController::class, 'index'])->name('data-unit');
    Route::get('data-unit/add', [DataUnitController::class, 'create'])->name('add-data-unit');
    Route::post('data-unit/add-simpan', [DataUnitController::class, 'store'])->name('simpan_asset');
    Route::get('/pesan-data-unit' , [MessageController::class, 'pesanDataUnit'])->name('pesan-data-unit');
    Route::get('/get-dataUnit-messages', [MessageController::class, 'getDataUnitMessages']);

    Route::get('/edit-data-unit/show/{id}', [MessageController::class, 'editDataUnit'])->name('edit-data-unit');
    Route::put('/edit-data-unit/update/{id}/{id_message}', [MessageController::class, 'update_pesanDataUnit'])->name('update_pesanDataUnit');
    Route::get('/riwayat-perubahan-dataUnit', [MessageController::class, 'riwayatDataUnit'])->name('riwayatPerubahanDataUnit');
    Route::get('/riwayat-dataUnit-json', [MessageController::class, 'riwayatDataUnitJson']);
    Route::post('/revisi-data-Unit/{id}', [MessageController::class, 'revisiAsset'])->name('revisi_pesan_asset');
    // Route::put('/ubah-data-Unit/{id}', [MessageController::class, 'revisiAsset'])->name('revisi_pesan_asset');
    // end route data unit

    //route pesan
    Route::get('/message-request', [MessageController::class, 'get_message_request'])->name('messageRequest');
    Route::get('/get-message-request-json', [MessageController::class, 'getMessageRequestJson']);


    //untuk manager
    Route::middleware(['role:manager'])->group(function () {
        // Rute-rute yang hanya dapat diakses oleh role "manager"
        Route::get('/manager-only', function () {
            return view('pages.manager.manager');
        })->name('manager.page');
    });

    //untuk supervisor
    Route::middleware(['role:supervisor'])->group(function () {
        // Rute-rute yang hanya dapat diakses oleh role "supervisor"
        Route::get('peminjaman/ubah{id}/{asset_id}', [OperationActiveController::class, 'edit_data'])->name('edit-peminjaman');
        Route::put('peminjaman/update/{id}', [OperationActiveController::class, 'update_data'])->name('update');
        Route::get('/setujui-operations-message/{id}', [MessageController::class, 'setujuOperationMessage']);
        Route::get('/setujui-maintenance-message/{id}', [MessageController::class, 'setujuMaintenanceMessage']);
        Route::get('/setujui-dataUnit-message/{id}', [MessageController::class, 'setujuDataUnitMessage']);
        Route::get('perbaikan/ubah{id}/{asset_id}', [MaintenanceActiveController::class, 'edit_data'])->name('edit-perbaikan');
        Route::put('perbaikan/update/{id}', [MaintenanceActiveController::class, 'update_data'])->name('update_perbaikan');
    });


    Route::get('logout', [Controller::class, 'logout'])->name('logout');
    Route::get('Profil', [Controller::class, 'profilView'])->name('profilView');
});

