<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AssetsModelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('assets')->insert([
            'id_type' => '1',
            'id_ctgr' => '2',
            'id_permits_insurance' => '1',
            'manufacture' => 'Avanza1',
            'model' => 'model1',
            'yom' => '2002',
            'no_unit' => '0021'
        ]);
        DB::table('assets')->insert([
            'id_type' => '2',
            'id_ctgr' => '2',
            'id_permits_insurance' => '2',
            'manufacture' => 'Avanza2',
            'model' => 'model2',
            'yom' => '2002',
            'no_unit' => '0013'
        ]);
        DB::table('assets')->insert([
            'id_type' => '3',
            'id_ctgr' => '2',
            'id_permits_insurance' => '3',
            'manufacture' => 'Avanza3',
            'model' => 'model3',
            'yom' => '2002',
            'no_unit' => '0045'
        ]);
        DB::table('assets')->insert([
            'id_type' => '4',
            'id_ctgr' => '2',
            'id_permits_insurance' => '4',
            'manufacture' => 'Avanza4',
            'model' => 'model14',
            'yom' => '2002',
            'no_unit' => '0015'
        ]);
    }
}
