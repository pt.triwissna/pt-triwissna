<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeInsuranceSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('type_insurance')->insert([
            'insurance_type_name' => 'All RISK'
        ]);
        DB::table('type_insurance')->insert([
            'insurance_type_name' => 'TLO'
        ]);
        DB::table('type_insurance')->insert([
            'insurance_type_name' => 'Comprehensive'
        ]);
    }
}
