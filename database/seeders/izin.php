<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class izin extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('permits_insurance')->insert([
            'id_insurance_type' => '2',
            'record_adm_id' => '6'
        ]);
        DB::table('permits_insurance')->insert([
            'id_insurance_type' => '1',
            'record_adm_id' => '6'
        ]);
        DB::table('permits_insurance')->insert([
            'id_insurance_type' => '3',
            'record_adm_id' => '6'
        ]);
        DB::table('permits_insurance')->insert([
            'id_insurance_type' => '2',
            'record_adm_id' => '6'
        ]);
    }
}
