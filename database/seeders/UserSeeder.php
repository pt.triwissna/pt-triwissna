<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'username' => 'TriAdmin',
            'role_id' => '1',
            'password' => Hash::make('password123'),
        ]);

        DB::table('users')->insert([
            'name' => 'supervisor',
            'username' => 'TriSupervisor',
            'role_id' => '2',
            'password' => Hash::make('password123'),
        ]);

        DB::table('users')->insert([
            'name' => 'manager',
            'username' => 'TriManager',
            'role_id' => '3',
            'password' => Hash::make('password123'),
        ]);
        

        //untuk roles
        DB::table('role')->insert([
            'role' => 'admin',
        ]);
        DB::table('role')->insert([
            'role' => 'supervisor',
        ]);
        DB::table('role')->insert([
            'role' => 'manajer',
        ]);
    }
}
