<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\AssetsModel;
use App\Models\PermitsInsuranceModels;
use Database\Factories\PermitsInsuranceFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call([
            UserSeeder::class,
            CategoryAssetSeed::class,
            TypeAssetSeed::class,
            TypeInsuranceSeed::class,
            // AssetsModelSeeder::class,
            // izin::class,
        ]);

    }
}
