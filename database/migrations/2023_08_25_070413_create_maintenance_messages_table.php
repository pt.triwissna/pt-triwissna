<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('maintenance_messages', function (Blueprint $table) {
            $table->id('id_maintenance_message');
            $table->date('date_message');
            $table->String('title_message');
            $table->text('contents_message');
            $table->foreignId('id_maintenance_active');
            $table->enum('flg_action', ['N', 'Y', 'H'])->default('N');
            $table->enum('for_to', ['admin', 'supervisor'])->default('supervisor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('maintenance_messages');
    }
};
