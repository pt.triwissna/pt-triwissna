<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('maintenance_history', function (Blueprint $table) {
            $table->id('maintenance_id');
            $table->unsignedBigInteger('asset_id');
            $table->date('s_breakdown_date');
            $table->time('s_breakdown_time');
            $table->date('f_breakdown_date')->nullable();
            $table->time('f_breakdown_time')->nullable();
            $table->text('issue');
            $table->string('perform_by');
            $table->bigInteger('finance')->nullable();
            $table->unsignedBigInteger('record_adm_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('maintenance_history');
    }
};
