<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->id('asset_id');
            $table->unsignedBigInteger('id_type');
            $table->unsignedBigInteger('id_ctgr');
            $table->foreignId('id_permits_insurance')->nullable();
            $table->string('manufacture');
            $table->string('model');
            $table->integer('yom');
            $table->string('no_unit')->unique();
            $table->enum('flg_status', ['T', 'B', 'P'])->default('T');
            $table->timestamps();

            $table->foreign('id_type')->references('id_type')->on('type_asset')->onDelete('cascade');
            $table->foreign('id_ctgr')->references('id_ctgr')->on('category_asset')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('asset');
    }
};