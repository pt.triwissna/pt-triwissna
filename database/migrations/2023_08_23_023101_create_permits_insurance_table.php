<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('permits_insurance', function (Blueprint $table) {
            $table->id('id_permits_insurance');
            $table->integer('stnk_issued')->nullable();
            $table->integer('stnk_expired')->nullable();
            $table->integer('kir_issued')->nullable();
            $table->integer('kir_expired')->nullable();
            $table->unsignedBigInteger('id_insurance_type')->nullable();
            $table->integer('insurance_issued')->nullable();
            $table->integer('insurance_expired')->nullable();
            $table->unsignedBigInteger('record_adm_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('permits_insurance');
    }
};
