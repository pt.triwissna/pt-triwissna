<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('operation_history', function (Blueprint $table) {
            $table->id('id_operation');
            $table->unsignedBigInteger('asset_id'); // Gunakan unsignedBigInteger
            $table->date('date');
            $table->enum('shift', ['DAY', 'NIGHT']);
            $table->integer('s_hourmeter')->nullable();
            $table->integer('f_hourmeter')->nullable();
            $table->time('s_work_time');
            $table->time('f_work_time')->nullable();
            $table->time('s_otherdelay')->nullable();
            $table->time('f_otherdelay')->nullable();
            $table->integer('s_odometer')->nullable();
            $table->integer('f_odometer')->nullable();
            $table->integer('fuel_inflow')->nullable();
            $table->text('project');
            $table->string('operator');
            $table->integer('trip')->nullable();
            $table->integer('record_adm_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('operation_history');
    }
};
