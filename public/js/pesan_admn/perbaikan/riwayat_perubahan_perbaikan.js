function fetchAndDisplayRiwayat() {
    // Ambil elemen dengan ID 'riwayat-perubahan-peminjaman-table'
    const tableContainer = document.getElementById('riwayat-perubahan-perbaikan-table');

    // Fetch data dari API atau sumber data lainnya
    fetch('/riwayat-perbaikan-json') // Ubah URL menjadi '/riwayat-perbaikan-json'
        .then(response => response.json())
        .then(data => {
            // Membersihkan elemen HTML sebelum menambahkan data yang baru
            tableContainer.innerHTML = '';

            console.log('Data yang diterima:', data);

            // Loop melalui data dan tampilkan dalam elemen tabel
            data.forEach(item => {
                const riwayatItem = document.createElement('div');
                riwayatItem.className = 'riwayat-item';

                // Menambahkan kolom untuk tanggal pesan
                const dateText = document.createElement('div');
                dateText.className = 'riwayat-date';
                dateText.textContent = `${item.date_message}`;

                // Menambahkan kolom untuk judul
                const titleText = document.createElement('div');
                titleText.className = 'riwayat-title';
                titleText.textContent = `Riwayat - ${item.title_message}`;

                // Menambahkan kolom untuk detail
                const detailText = document.createElement('div');
                detailText.className = 'riwayat-detail';
                detailText.textContent = `${item.pesan}`;

                riwayatItem.appendChild(dateText);
                riwayatItem.appendChild(titleText);
                riwayatItem.appendChild(detailText);

                // Menambahkan item ke dalam tabel
                tableContainer.appendChild(riwayatItem);
            });
        })
        .catch(error => {
            console.error('Gagal mengambil data:', error);
        });
}

// Panggil fungsi fetchAndDisplayRiwayat untuk pertama kali
fetchAndDisplayRiwayat();

// Atur interval untuk memperbarui data setiap beberapa detik
setInterval(fetchAndDisplayRiwayat, 3000);
