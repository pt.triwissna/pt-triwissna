function fetchAndDisplayPermintaan() {
    // Dalam permintaan.js

    // Ambil elemen dengan ID 'permintaan-peminjaman-list'
    const listContainer = document.getElementById("permintaan-peminjaman-list");

    // Fetch data dari API atau sumber data lainnya
    fetch("/get-message-request-json")
        .then((response) => response.json())
        .then((data) => {
            // Gabungkan data dari kedua jenis model
            const mergedData = [
                ...data.operations,
                ...data.maintenance,
                ...data.asset,
            ];

            // Urutkan data berdasarkan tanggal dengan menggunakan timestamp
            mergedData.sort((a, b) => {
                return (
                    new Date(b.date_message).getTime() -
                    new Date(a.date_message).getTime()
                );
            });

            // Membersihkan elemen HTML sebelum menambahkan data yang baru
            listContainer.innerHTML = "";
            const userRole = loggedInUser.role.role;

            // Loop melalui data dan tampilkan dalam elemen list
            mergedData.forEach((item) => {
                const listItem = document.createElement("div");
                listItem.className = "list-item";

                // Tambahkan konten untuk setiap item
                const itemContent = document.createElement("div");
                itemContent.className = "item-content";

                const dateText = document.createElement("span");
                dateText.className = "date-text";
                dateText.textContent = `${item.date_message} : Permintaan - ${item.title_message}`;
                itemContent.appendChild(dateText);
                if (userRole === 'supervisor') {
                const approveButton = document.createElement("button");
                approveButton.className = "btn btn-success btn-sm";
                approveButton.textContent = "Setujui";
                approveButton.addEventListener("click", () => {
                    let route = "";
                    if (item.id_operations_message) {
                        route = `/setujui-operations-message/${item.id_operations_message}`;
                    } else if (item.id_maintenance_message) {
                        route = `/setujui-maintenance-message/${item.id_maintenance_message}`;
                    } else if (item.id_dataUnit_message) {
                        route = `/setujui-dataUnit-message/${item.id_dataUnit_message}`;
                    }

                    approveButton.setAttribute("href", route); // Set atribut href dengan rute yang sesuai
                    approveButton.addEventListener("click", (event) => {
                        event.preventDefault(); // Hentikan navigasi standar
                        window.location.href = route; // Arahkan ke rute yang sesuai
                    });
                });
                itemContent.appendChild(approveButton);
            }

                listItem.appendChild(itemContent);

                // Tambahkan item ke dalam daftar
                listContainer.appendChild(listItem);
            });
        })
        .catch((error) => {
            console.error("Gagal mengambil data:", error);
        });
}

// Panggil fungsi fetchAndDisplayPermintaan untuk pertama kali
fetchAndDisplayPermintaan();

// Atur interval untuk memperbarui data setiap beberapa detik
setInterval(fetchAndDisplayPermintaan, 3000); // Misalnya, pembaruan setiap 10 detik
