<!-- Footer -->
<footer class="content-footer footer bg-footer-theme">
    <div
        class="container-xxl d-flex flex-wrap justify-content-between py-2 flex-md-row flex-column">
        <div class="mb-2 mb-md-0">
            ©
            <script>
                document.write(new Date().getFullYear());
            </script>
            , made by RPL
            <a href="https://smkbagimunegeriku.sch.id/" target="_blank"
                class="footer-link fw-bolder">SMK BAGIMU NEGERIKU</a>
        </div>
    </div>
</footer>
<!-- / Footer -->