<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand-demo">
        <a href="#" class="app-brand-link">
            <div class="app-brand-image-container">
                <img src="{{ asset('img/logo.png') }}" alt="Logo Sneat" class="app-brand-image">
            </div>
        </a>
        <span class="app-brand-text demo">PT Triwisnna</span>
        {{-- class logo --}}
        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>
    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py2">
        <!-- Dashboard -->
        <li class="menu-item {{ request()->routeIs('home') ? 'active' : '' }}">
            <a href="/" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Beranda</div>
            </a>
        </li>


        <li class="menu-header small text-uppercase">
            <span class="menu-header-text">Pages</span>
        </li>
        <li class="menu-item {{ request()->routeIs('peminjaman-aktif', 'peminjaman-tambah-data', 'in-pinjam-selesai', 'move-data', 'riwayat-peminjaman') ? 'active open' : '' }}">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-detail"></i>
                <div data-i18n="peminjaman">Peminjaman</div>
            </a>
            <ul class="menu-sub">
              <li class="menu-item {{ request()->routeIs('peminjaman-aktif', 'peminjaman-tambah-data', 'in-pinjam-selesai', 'move-data') ? 'active' : '' }}">
                <a href="{{ route('peminjaman-aktif') }}" class="menu-link">
                  <div data-i18n="peminjaman-aktif">Peminjaman Aktif</div>
                </a>
              </li>
              <li class="menu-item {{ request()->routeIs('riwayat-peminjaman') ? 'active' : '' }}">
                <a href="{{ route('riwayat-peminjaman') }}" class="menu-link">
                    <div data-i18n="riwayat-peminjaman">Riwayat Peminjaman</div>
                </a>
            </li>            
            </ul>
        </li>
        <li class="menu-item {{ request()->routeIs('perbaikan-aktif', 'selesai-perbaikan', 'riwayat-perbaikan', 'add-perbaikan-aktif') ? 'active open' : '' }}">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-detail"></i>
                <div data-i18n="perbaikan">Perbaikan</div>
            </a>
            <ul class="menu-sub">
                <li class="menu-item {{ request()->routeIs('perbaikan-aktif', 'selesai-perbaikan', 'add-perbaikan-aktif') ? 'active' : '' }}">
                    <a href="{{ route('perbaikan-aktif') }}" class="menu-link">
                        <div data-i18n="perbaikan-aktif">Perbaikan Aktif</div>
                    </a>
                </li>
                <li class="menu-item {{ request()->routeIs('riwayat-perbaikan') ? 'active' : '' }}">
                    <a href="{{ route('riwayat-perbaikan') }}" class="menu-link">
                        <div data-i18n="riwayat-perbaikan">Riwayat Perbaikan</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="menu-item {{ request()->routeIs('data-unit', 'add-data-unit') ? 'active' : '' }}">
            <a href="{{ route('data-unit') }}" class="menu-link">
                <i class="menu-icon tf-icons bx bx-table"></i>
                <div data-i18n="data-unit">Data Unit</div>
            </a>
        </li>
    </ul>
</aside>
