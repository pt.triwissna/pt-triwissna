@extends('app')
@section('title-app')
    Perbaikan Aktif
@endsection
@section('navbar-title-back')
    Perbaikan Aktif
@endsection
@section('content')
    @if (session('success'))
        @include('partials.alert-success')
    @endif

    @if (session('error'))
        @include('partials.alert-error')
    @endif
    <div class="card p-3">
        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            @if ($loggedInUser->role->role == 'admin')
                <a href="{{ route('add-perbaikan-aktif') }}" class="btn btn-primary me-md-2 pe-5 ps-5">Tambah</a>
            @endif
        </div>
        <form action="{{ route('perbaikan-aktif') }}" method="GET">
            @csrf
            @if ($loggedInUser->role->role != 'admin')
                <div class="row mb-5 mt-5">
                @else
                    <div class="row mb-5">
            @endif
            {{-- <div class="row mt-4 mb-5"> --}}
            <div class="col-md-3">
                <div>
                    <label for="filter" class="fw-bold">Nomor Unit</label>
                    <input name="no_unit" type="text" class="form-control" id="defaultFormControlInput"
                        placeholder="Nomor Unit" aria-describedby="defaultFormControlHelp"
                        value="{{ isset($_GET['no_unit']) ? $_GET['no_unit'] : '' }}" />
                </div>
            </div>
            <div class="col-md-3">
                <div>
                    <label for="date" class="fw-bold">Tanggal Mulai</label>
                    <input name="date" type="date" class="form-control" id="defaultFormControlInput"
                        placeholder="Nomor Unit" aria-describedby="defaultFormControlHelp"
                        value="{{ isset($_GET['date']) ? $_GET['date'] : '' }}" />
                </div>
            </div>
            <div class="col-md-3">
                <div>
                    <label for="filter" class="fw-bold">Tipe</label>
                    <select name="id_type" class="form-select" id="exampleFormControlSelect1"
                        aria-label="Default select example">
                        <option selected value="">- Semua -</option>
                        @foreach ($typeAssets as $o)
                            <option value="{{ $o->id_type }}"
                                {{ isset($_GET['id_type']) && (int) $_GET['id_type'] === $o->id_type ? 'selected' : '' }}>
                                {{ $o->type_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-1  d-flex align-items-end">
                <button type="submit" class="btn btn-primary">Cari</button>
            </div>
    </div>
    </form>
    <div class="table-responsive text-nowrap">
        <table class="table table-striped table-hover">
            <thead>
                <tr class="table-active">
                    <th class="fw-bold">Aksi</th>
                    <th class="fw-bold">Tanggal Mulai</th>
                    <th class="fw-bold">No. Unit</th>
                    <th class="fw-bold">Tipe</th>
                    <th class="fw-bold">Kategori</th>
                    <th class="fw-bold">Dilakukan Oleh</th>
                    <th class="fw-bold">Masalah</th>
                </tr>
            </thead>
            <tbody class="table-border-bottom-0">
                @if (count($maintenanceActive) < 1)
                    <tr>
                        <td colspan="7" style="padding: 20px; font-size: 20px;"><span>Tidak ditemukan data</span>
                        </td>
                    </tr>
                @else
                    @foreach ($maintenanceActive as $index)
                        @include('pages.admin.perbaikan_aktif.modals.detail_perbaikan_aktif')
                        @include('pages.admin.perbaikan_aktif.modals.revisi_perbaikan_aktif')
                        @include('pages.admin.perbaikan_aktif.modals.selesai_perbaikan_aktif')
                        <tr>
                            <td>
                                <button class="btn btn-primary btn-sm" type="button" data-bs-toggle="modal"
                                    data-bs-target="#datailperbaikanaktif{{ $index->maintenance_active_id }}">DETAIL</button>
                                @if ($loggedInUser->role->role == 'admin' || $loggedInUser->role->role == 'supervisor')
                                @if ($index->flg_action == 'H' or $index->flg_action == null)
                                <button class="btn btn-danger btn-sm" type="button" data-bs-toggle="modal"
                                    data-bs-target="#revisiPerbaikanAktif{{ $index->maintenance_active_id }} ">REVISI</button>
                            @else
                                <button class="btn btn-danger btn-sm" type="button" data-bs-toggle="modal"
                                    data-bs-target="#revisiPerbaikanAktif{{ $index->maintenance_active_id }}"
                                    disabled>REVISI</button>
                                @endif
                               
                                @endif
                                @if ($loggedInUser->role->role == 'admin')
                                    @if ($index->flg_action == 'H' || $index->flg_action == null)
                                        <button class="btn btn-success btn-sm" type="button" data-bs-toggle="modal"
                                            data-bs-target="#selesaiPerbaikanAktif{{ $index->maintenance_active_id }}">SELESAI</button>
                                    @else
                                        <button class="btn btn-success btn-sm" type="button" data-bs-toggle="modal"
                                            data-bs-target="#selesaiPerbaikanAktif{{ $index->maintenance_active_id }}"
                                            disabled>SELESAI</button>
                                    @endif
                                @endif
                                @if ($loggedInUser->role->role == 'supervisor')
                                    <a href="{{ route('edit-perbaikan', ['id' => $index->maintenance_active_id, 'asset_id' =>$index->asset_id ]) }}"
                                        class="btn btn-warning btn-sm">UBAH</a>
                                @endif
                            </td>
                            <td>{{ $index->s_breakdown_date }}</td>
                            <td>{{ $index->no_unit }}</td>
                            <td>{{ $index->type_name }}</td>
                            <td>{{ $index->ctgr_name }}</td>
                            <td>{{ $index->perform_by }}</td>
                            <td class="text-wrap">{{ $index->issue }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="row pt-5">
        <div class="col-lg-10">
            <br>
            <span>Total Perbaikan Aktif {{ $total[0]->totalData }}</span>
        </div>
    </div>
    </div>
    </div>
@endsection
