@php
    // Import class Carbon
    use Carbon\Carbon;

    // Set locale ke bahasa Indonesia
    Carbon::setLocale('id');

    // Mendapatkan nama hari dalam bahasa Indonesia
    $dayInIndonesian = Carbon::parse($index->s_breakdown_date)->translatedFormat('l');
    $monthsInIndonesian = [
        1 => 'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember',
    ];

    // Ubah format tampilan tanggal
    $date = Carbon::createFromFormat('Y-m-d', $index->s_breakdown_date); // Ganti dengan tanggal yang sesuai
    $formattedDate = $date->format('d') . ' ' . $monthsInIndonesian[$date->format('n')] . ' ' . $date->format('Y');
@endphp

<div class="modal fade" id="datailperbaikanaktif{{ $index->maintenance_active_id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel3">Detail Perbaikan Aktif</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <hr>
            <div class="modal-body">
                <form>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">Tanggal Mulai</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $dayInIndonesian }}, {{ $formattedDate ?? '' }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">Jam Mulai</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $index->s_breakdown_time }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">Masalah</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $index->issue }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">Dilakukan Oleh</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $index->perform_by }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">No. Unit</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $index->no_unit }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">Tipe</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $index->type_name }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">Kategori</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $index->ctgr_name }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">Merek</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $index->manufacture }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">Model</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $index->model }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">Tahun</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $index->yom }}"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
