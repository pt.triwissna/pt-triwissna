<div class="modal fade" id="selesaiPerbaikanAktif{{ $index->maintenance_active_id }}" aria-hidden="true" aria-labelledby="modalToggleLabel2" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalToggleLabel2">Selesai Perbaikan</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <hr class="line">
            <form action="{{ route('selesai-perbaikan', ['maintenance_active_id' => $index->maintenance_active_id]) }}"
                method="post">
                @csrf
                <div class="modal-body">
                    <div class="row mb-3">
                        <div class="col-md-4">
                            <label for="tanggal-selesai" class="form-label">Tanggal Selesai</label>
                        </div>
                        <div class="col-md-8">
                            <input name="f_breakdown_date" type="date" id="tanggal-selesai" class="form-control">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-4">
                            <label for="jam-selesai" class="form-label">Jam Selesai</label>
                        </div>
                        <div class="col-md-8">
                            <input name="f_breakdown_time" type="time" id="jam-selesai" class="form-control">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-4">
                            <label for="biaya" class="form-label">Biaya (Rp)</label>
                        </div>
                        <div class="col-md-8">
                            <input name="finance" type="number" id="biaya" class="form-control">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button class="btn btn-info" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Selesai</button>
                </div>

            </form>
        </div>
    </div>
</div>