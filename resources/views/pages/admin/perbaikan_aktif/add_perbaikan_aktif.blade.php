@extends('app')
@section('title-app')
    Tambah Perbaikan Aktif
@endsection
@section('navbar-title-back')
    Perbaikan Aktif
@endsection
@section('navbar-title-target')
    / Tambah
@endsection
@section('link-back')
    {{ route('perbaikan-aktif') }}
@endsection
@section('content')
    <div class="card p-3">
        <div class="container-view">
            <a href="/perbaikan-aktif">
                <button type="button" class="btn btn-primary">Kembali</button>
            </a>
            <hr>
            <br>
            <form action="{{ route('insert_maintenance') }}" method="post">
                @csrf
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Tanggal Mulai</label>
                    <div class="col-sm-6">
                        <input type="date" name="s_breakdown_date"
                            class="form-control @error('s_breakdown_date') is-invalid @enderror" id="basic-default-name"
                            value="{{ old('s_breakdown_date') }}" />
                        @error('s_breakdown_date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Jam Mulai</label>
                    <div class="col-sm-6">
                        <input type="time" name="s_breakdown_time"
                            class="form-control @error('s_breakdown_time') is-invalid @enderror" id="basic-default-name"
                            value="{{ old('s_breakdown_time') }}" />
                        @error('s_breakdown_time')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Unit</label>
                    <div class="col-sm-6">
                        <select name="asset_id" class="form-select @error('asset_id') is-invalid @enderror" id="exampleFormControlSelect1"
                            aria-label="Default select example">
                            <option></option>
                            @foreach ($getAsset as $a)
                                <option value="{{ $a->asset_id }}" {{ old('asset_id') == $a->asset_id ? 'selected' : '' }}>{{ $a->no_unit }} - {{ $a->manufacture }}</option>
                            @endforeach
                        </select>
                        @error('asset_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Masalah</label>
                    <div class="col-sm-6">
                        <input type="text" name="issue" class="form-control @error('issue') is-invalid @enderror"
                            id="basic-default-name" value="{{ old('issue') }}" />
                        @error('issue')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>

                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Dilakukan Oleh</label>
                    <div class="col-sm-6">
                        <input type="text" name="perform_by"
                            class="form-control @error('perform_by') is-invalid @enderror" id="basic-default-name"
                            value="{{ old('perform_by') }}" />
                        @error('perform_by')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-end mt-2 mb-5">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
