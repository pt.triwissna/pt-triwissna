@extends('app')
@section('title-app')
    Tambah Perbaikan Aktif
@endsection
@section('navbar-title-back')
    Perbaikan Aktif
@endsection
@section('navbar-title-target')
    / Ubah
@endsection
@section('link-back')
    {{ route('perbaikan-aktif') }}
@endsection


@section('content')
<div class="card p-3">
    <div class="container-view">
        <a href="/perbaikan-aktif">
            <button type="button" class="btn btn-primary">Kembali</button>
        </a>
        <hr>
        <br>
        <form action="{{ route('update_perbaikan', ['id' => $maintenanceActive->maintenance_active_id]) }}" method="post">
            @csrf
            @method('PUT')
            <div class="row mb-3 justify-content-start mb-3">
                <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Tanggal Mulai</label>
                <div class="col-sm-6">
                    <input type="date" name="s_breakdown_date"
                        class="form-control @error('s_breakdown_date') is-invalid @enderror" id="basic-default-name"
                        value="{{ old('s_breakdown_date', $maintenanceActive->s_breakdown_date) }}" />
                    @error('s_breakdown_date')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
            <div class="row mb-3 justify-content-start mb-3">
                <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Jam Mulai</label>
                <div class="col-sm-6">
                    <input type="time" name="s_breakdown_time"
                        class="form-control @error('s_breakdown_time') is-invalid @enderror" id="basic-default-name"
                        value="{{ old('s_breakdown_time', $maintenanceActive->s_breakdown_time) }}" />
                    @error('s_breakdown_time')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
            <div class="row mb-3 justify-content-start mb-3">
                <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Unit</label>
                <div class="col-sm-6">
                    <select name="asset_id" class="form-select" id="exampleFormControlSelect1"
                        aria-label="Default select example">
                        <option value="{{ $getAsset->asset_id }}">{{ $getAsset->no_unit }} - {{ $getAsset->manufacture }} </option>
                    </select>
                </div>
            </div>
            <div class="row mb-3 justify-content-start mb-3">
                <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Masalah</label>
                <div class="col-sm-6">
                    <input type="text" name="issue" class="form-control @error('issue') is-invalid @enderror"
                        id="basic-default-name" value="{{ old('issue', $maintenanceActive->issue) }}" />
                    @error('issue')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>

            <div class="row mb-3 justify-content-start mb-3">
                <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Dilakukan Oleh</label>
                <div class="col-sm-6">
                    <input type="text" name="perform_by"
                        class="form-control @error('perform_by') is-invalid @enderror" id="basic-default-name"
                        value="{{ old('perform_by', $maintenanceActive->perform_by) }}" />
                    @error('perform_by')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>
            <div class="row justify-content-end mt-2 mb-5">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
