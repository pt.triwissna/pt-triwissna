@extends('app')
@section('title-app')
    Beranda
@endsection
@section('navbar-title-back')
    Beranda
@endsection
@section('content')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <div class="col-lg-8 mb-2 order-0">
                <div class="card">
                    <div class="d-flex align-items-end row">
                        <div class="col-sm-7">
                            <div class="card-body">
                                <h4 class="card-title text-primary">Selamat Datang {{ Auth::user()->name }}</h4>
                                <span>{{ \Carbon\Carbon::now()->locale('id')->translatedFormat('l, d F Y') }}</span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 order-1">
                <div class="row">
                    <div class="col-lg-6 col-md-12 col-6 mb-4">
                        <div class="card">
                            <div class="card-body text-center">
                                <span class="fw-semibold d-block mb-1">Total Peminjaman</span>
                                <h3 class="card-title mb-2 text-success">{{ $operation[0]->total_operation }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 col-6 mb-4">
                        <div class="card">
                            <div class="card-body text-center">
                                <span class="fw-semibold d-block mb-1">Total Perbaikan</span>
                                <h3 class="card-title mb-2 text-success">{{ $maintenace[0]->total_maintenace }}</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @if ($loggedInUser->role->role == 'admin')
                <div class="col-12 col-lg-12 order-2 order-md-3 order-lg-2 mb-4">
                    <div class="card">
                        <div class="row row-bordered g-0">
                            <div class="col-md-12 p-3">
                                <h5 class="card-header">Revisi Peminjaman</h5>
                                <div style="height: 250px; overflow-x: scroll">
                                    <div class="card-body">
                                        @if (count($getOperationsAdm) < 1)
                                            <div class="alert alert-secondary" role="alert">
                                                Tidak ada Revisi Peminjaman.
                                            </div>
                                        @else
                                            @foreach ($getOperationsAdm as $operationAdm)
                                                <div class="alert alert-primary operation-alert d-flex flex-column mb-3"
                                                    role="alert">
                                                    <span
                                                        class="alert-title fw-bold">{{ $operationAdm->title_message }}</span>
                                                    <span
                                                        class="alert-content flex-grow-1 overflow-hidden text-truncate">{{ $operationAdm->contents_message }}</span>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ Total Revenue -->
                <div class="col-12 col-lg-12 order-2 order-md-3 order-lg-2 mb-4">
                    <div class="card">
                        <div class="row row-bordered g-0">
                            <div class="col-md-12 p-3">
                                <h5 class="card-header">Revisi Perbaikan</h5>
                                <div style="height: 250px; overflow-x: scroll">
                                    <div class="card-body">
                                        @if (count($getMaintenanceAdm) < 1)
                                            <div class="alert alert-secondary" role="alert">
                                                Tidak ada Revisi Perbaikan.
                                            </div>
                                        @else
                                            @foreach ($getMaintenanceAdm as $maintenanceAdm)
                                                <div class="alert alert-primary operation-alert d-flex flex-column mb-3"
                                                    role="alert">
                                                    <span
                                                        class="alert-title fw-bold">{{ $maintenanceAdm->title_message }}</span>
                                                    <span
                                                        class="alert-content flex-grow-1 overflow-hidden text-truncate">{{ $maintenanceAdm->contents_message }}</span>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-12 order-2 order-md-3 order-lg-2 mb-4">
                    <div class="card">
                        <div class="row row-bordered g-0">
                            <div class="col-md-12 p-3">
                                <h5 class="card-header">Revisi Data Unit</h5>
                                <div style="height: 250px; overflow-x: scroll">
                                    <div class="card-body">
                                        @if (count($getDataUnitAdm) < 1)
                                            <div class="alert alert-secondary" role="alert">
                                                Tidak ada Permintaan Revisi Perbaikan.
                                            </div>
                                        @else
                                            @foreach ($getDataUnitAdm as $dataUnitAdm)
                                                <div class="alert alert-primary operation-alert d-flex flex-column mb-3"
                                                    role="alert">
                                                    <span
                                                        class="alert-title fw-bold">{{ $dataUnitAdm->title_message }}</span>
                                                    <span
                                                        class="alert-content flex-grow-1 overflow-hidden text-truncate">{{ $dataUnitAdm->contents_message }}</span>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            @if ($loggedInUser->role->role == 'supervisor')
                <div class="col-12 col-lg-12 order-2 order-md-3 order-lg-2 mb-4">
                    <div class="card">
                        <div class="row row-bordered g-0">
                            <div class="col-md-12 p-3">
                                <h5 class="card-header">Permintaan Revisi Peminjaman</h5>
                                <div style="height: 250px; overflow-x: scroll">
                                    <div class="card-body">
                                        @if (count($getOperationsSpv) < 1)
                                            <div class="alert alert-secondary" role="alert">
                                                Tidak ada Permintaan Revisi Peminjaman.
                                            </div>
                                        @else
                                            @foreach ($getOperationsSpv as $operationSpv)
                                                <div class="alert alert-primary operation-alert d-flex flex-column mb-3"
                                                    role="alert">
                                                    <span
                                                        class="alert-title fw-bold">{{ $operationSpv->title_message }}</span>
                                                    <span
                                                        class="alert-content flex-grow-1 overflow-hidden text-truncate">{{ $operationSpv->contents_message }}</span>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ Total Revenue -->
                <div class="col-12 col-lg-12 order-2 order-md-3 order-lg-2 mb-4">
                    <div class="card">
                        <div class="row row-bordered g-0">
                            <div class="col-md-12 p-3">
                                <h5 class="card-header">Permintaan Revisi Perbaikan</h5>
                                <div style="height: 250px; overflow-x: scroll">
                                    <div class="card-body">
                                        @if (count($getMaintenanceSpv) < 1)
                                            <div class="alert alert-secondary" role="alert">
                                                Tidak ada Permintaan Revisi Perbaikan.
                                            </div>
                                        @else
                                            @foreach ($getMaintenanceSpv as $maintenanceSpv)
                                                <div class="alert alert-primary operation-alert d-flex flex-column mb-3"
                                                    role="alert">
                                                    <span
                                                        class="alert-title fw-bold">{{ $maintenanceSpv->title_message }}</span>
                                                    <span
                                                        class="alert-content flex-grow-1 overflow-hidden text-truncate">{{ $maintenanceSpv->contents_message }}</span>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-12 order-2 order-md-3 order-lg-2 mb-4">
                    <div class="card">
                        <div class="row row-bordered g-0">
                            <div class="col-md-12 p-3">
                                <h5 class="card-header">Permintaan Revisi Data Unit</h5>
                                <div style="height: 250px; overflow-x: scroll">
                                    <div class="card-body">
                                        @if (count($getDataUnitSpv) < 1)
                                            <div class="alert alert-secondary" role="alert">
                                                Tidak ada Permintaan Revisi Perbaikan.
                                            </div>
                                        @else
                                            @foreach ($getDataUnitSpv as $dataUnitSpv)
                                                <div class="alert alert-primary operation-alert d-flex flex-column mb-3"
                                                    role="alert">
                                                    <span
                                                        class="alert-title fw-bold">{{ $dataUnitSpv->title_message }}</span>
                                                    <span
                                                        class="alert-content flex-grow-1 overflow-hidden text-truncate">{{ $dataUnitSpv->contents_message }}</span>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <script src="{{ asset('js/total_peminjaman-perbaikan.js') }}"></script>
@endsection
