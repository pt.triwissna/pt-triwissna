@php
    // Import class Carbon
    use Carbon\Carbon;

    // Set locale ke bahasa Indonesia
    Carbon::setLocale('id');

    // Mendapatkan nama hari dalam bahasa Indonesia
    $dayInIndonesian = Carbon::parse($index->s_breakdown_date)->translatedFormat('l');
    $dayInIndonesian1 = Carbon::parse($index->f_breakdown_date)->translatedFormat('l');
    $monthsInIndonesian = [
        1 => 'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember',
    ];

    // Ubah format tampilan tanggal
    $date = Carbon::createFromFormat('Y-m-d', $index->s_breakdown_date); // Ganti dengan tanggal yang sesuai
    $date1 = Carbon::createFromFormat('Y-m-d', $index->f_breakdown_date); // Ganti dengan tanggal yang sesuai
    $formattedDate = $date->format('d') . ' ' . $monthsInIndonesian[$date->format('n')] . ' ' . $date->format('Y');
    $formattedDate1 = $date1->format('d') . ' ' . $monthsInIndonesian[$date1->format('n')] . ' ' . $date1->format('Y');
@endphp

<div class="modal fade" id="detailModelRiwayatPerbaikan{{ $index->maintenance_id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel3">Detail Riwayat Perbaikan</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="card-btn d-flex justify-content-end">
                <a href="{{ route('cetakPerbaikan.pdf', ['id' => $index->maintenance_id]) }}" class="btn btn-primary" style="margin-right: 25px;">Cetak PDF</a>
            </div>
            <hr>
            <div class="modal-body">
                <form>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Waktu Mulai</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $dayInIndonesian }}, {{ $formattedDate ?? '' }} | {{ $index->s_breakdown_time }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Waktu Selesai</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $dayInIndonesian1 }}, {{ $formattedDate1 ?? '' }} | {{ $index->f_breakdown_time }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Masalah</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $index->issue }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Biaya</label>
                        <div class="col-sm-9">
                            <?php
                            $formattedFinance = 'Rp ' . number_format($index->finance, 0, ',', '.');
                            ?>
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $formattedFinance }}"/>
                        </div>
                    </div>                    
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Dilakukan Oleh</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $index->perform_by }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">No. Unit</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $index->no_unit }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Tipe</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $index->type_name }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Merek</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $index->ctgr_name }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Model</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $index->model }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-3 col-form-label" for="basic-default-name">Tahun</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $index->yom }}"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
