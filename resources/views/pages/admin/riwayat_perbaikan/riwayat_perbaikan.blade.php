@extends('app')
@section('title-app')
    Riwayat Perbaikan
@endsection
@section('navbar-title-back')
    Riwayat Perbaikan
@endsection
@section('content')
    <div class="card p-3">
        <form action="{{ route('riwayat-perbaikan') }}" method="GET">
            <div class="row mt-4 mb-5">
                <div class="col-md-3">
                    <div>
                        <label for="filter" class="fw-bold">Nomor Unit</label>
                        <input type="text" class="form-control" id="defaultFormControlInput" placeholder="Nomor Unit"
                            aria-describedby="defaultFormControlHelp" name="no_unit"
                            value="{{ isset($_GET['no_unit']) ? $_GET['no_unit'] : '' }}" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div>
                        <label for="date" class="fw-bold">Tanggal Mulai</label>
                        <input type="date" class="form-control" id="defaultFormControlInput"
                            aria-describedby="defaultFormControlHelp" name="s_breakdown_date"
                            value="{{ isset($_GET['s_breakdown_date']) ? $_GET['s_breakdown_date'] : '' }}" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div>
                        <label for="filter" class="fw-bold">Tipe</label>
                        <select name="id_type" class="form-select" id="exampleFormControlSelect1"
                            aria-label="Default select example">
                            <option selected value="">- Semua -</option>
                            @foreach ($typeAssets as $o)
                                <option value="{{ $o->id_type }}" 
                                    {{ isset($_GET['id_type']) && (int) $_GET['id_type'] === $o->id_type ? 'selected' : '' }}>
                                    {{ $o->type_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-1  d-flex align-items-end">
                    <button type="submit" class="btn btn-primary">Cari</button>
                </div>
            </div>
        </form>
        <div class="table-responsive text-nowrap">
            <table class="table table-striped table-hover">
                <thead>
                    <tr class="table-active">
                        <th class="fw-bold">Aksi</th>
                        <th class="fw-bold">Tanggal</th>
                        <th class="fw-bold">No. Unit</th>
                        <th class="fw-bold">Tipe</th>
                        <th class="fw-bold">Kategori</th>
                        <th class="fw-bold">Dilakukan Oleh</th>
                        <th class="fw-bold">Masalah</th>
                    </tr>
                </thead>
                <tbody class="table-border-bottom-0">

                    @if (count($MaintenanceHistory) < 1)
                        <tr>
                            <td colspan="7" style="padding: 20px; font-size: 20px;"><span>Tidak ditemukan data</span>
                            </td>
                        </tr>
                    @else
                        @foreach ($MaintenanceHistory as $index)
                        @include('pages.admin.riwayat_perbaikan.detail_riwayat_perbaikan')
                            <tr>
                                <td>
                                    <button class="btn btn-primary btn-sm" type="button" data-bs-toggle="modal"
                                        data-bs-target="#detailModelRiwayatPerbaikan{{ $index->maintenance_id }}">DETAIL</button>
                                </td>
                                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $index->s_breakdown_date)->format('d-m-Y') }}
                                    /
                                    {{ \Carbon\Carbon::createFromFormat('Y-m-d', $index->f_breakdown_date)->format('d-m-Y') }}
                                </td>
                                <td>{{ $index->no_unit }}</td>
                                <td>{{ $index->type_name }}</td>
                                <td>{{ $index->ctgr_name }}</td>
                                <td>{{ $index->perform_by }}</td>
                                <td class="text-wrap">{{ $index->issue }}</td>
                            </tr>
                        @endforeach
                    @endif

                </tbody>
            </table>
        </div>

        <div class="row pt-5">
            <div class="col-lg-10">
                <ul class="pagination">
                    {{ $MaintenanceHistory->links() }}
                </ul>
                <br>
                <span>Total data {{ $totalData[0]->total_count }}, halaman {{ $MaintenanceHistory->currentPage() }} dari
                    {{ $MaintenanceHistory->lastPage() }}</span>

            </div>
        </div>
    </div>
@endsection
