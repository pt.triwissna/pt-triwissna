<!DOCTYPE html>
<html>

<head>
    <title>Detail Riwayat Peminjaman</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }

        h1 {
            font-size: 24px;
            margin-bottom: 20px;
        }

        h3 {
            font-size: 18px;
            margin-top: 10px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        th,
        td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #f2f2f2;
        }

        hr {
            border: 1px solid #ccc;
            margin: 20px 0;
        }
    </style>
</head>

<body>
    <h1>Detail Riwayat Perbaikan</h1>
    <hr>
    <table>
        <tr>
            <th>Waktu Mulai</th>
            <td>
                {{ \Carbon\Carbon::parse($maintenancehistory->s_breakdown_date)->locale('id')->isoFormat('dddd, D MMMM YYYY') }}
                | {{ \Carbon\Carbon::parse($maintenancehistory->s_breakdown_time)->format('H:i:s') }}
            </td>
        </tr>
        <tr>
            <th>Waktu Selesai</th>
            <td>
                {{ \Carbon\Carbon::parse($maintenancehistory->f_breakdown_date)->locale('id')->isoFormat('dddd, D MMMM YYYY') }}
                | {{ \Carbon\Carbon::parse($maintenancehistory->f_breakdown_time)->format('H:i:s') }}
            </td>
        </tr>
        <tr>
            <th>Masalah</th>
            <td>
                {{ $maintenancehistory->issue }}
            </td>
        </tr>
        <tr>
            <th>Biaya</th>
            <td>
                <?php
                $formattedFinance = 'Rp ' . number_format($maintenancehistory->finance, 0, ',', '.');
                ?>
                {{ $formattedFinance }}
            </td>
        </tr>
        <tr>
            <th>Dilakukan Oleh</th>
            <td>{{ $maintenancehistory->perform_by }}</td>
        </tr>
        <tr>
            <th>No. Unit</th>
            <td>{{ $maintenancehistory->asset->no_unit }}</td>
        </tr>
        <tr>
            <th>Tipe</th>
            <td>{{ $typeAsset->type_name }}</td>
        </tr>
        <tr>
            <th>Merek</th>
            <td>{{ $maintenancehistory->asset->manufacture }}</td>
        </tr>
        <tr>
            <th>Model</th>
            <td>{{ $maintenancehistory->asset->model }}</td>
        </tr>
        <tr>
            <th>Tahun</th>
            <td>{{ $maintenancehistory->asset->yom }}</td>
        </tr>
    </table>
</body>

</html>