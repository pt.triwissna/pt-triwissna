<div class="modal fade" id="selesai{{ $operationActive->id_operation_active }}" aria-hidden="true"
    aria-labelledby="modalToggleLabel2" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalToggleLabel2">Selesai Peminjaman</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <hr class="line">
            <form
                action="{{ route('in-pinjam-selesai', ['id_operation_active' => $operationActive->id_operation_active]) }}"
                method="post">
                @csrf
                <div class="modal-body">
                    <div class="row mb-3">
                        <div class="col-md-4">
                            <label for="jam-selesai" class="form-label">Jam Selesai</label>
                        </div>
                        <div class="col-md-8">
                            <input type="time" id="jam-selesai" class="form-control" name="f_work_time">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-4">
                            <label for="odometer" class="form-label">Odometer (mil)</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="odometer" class="form-control" name="f_odometer" 
                            @if ($operationActive->s_odometer)
                                required
                            @else 
                                @readonly(true)
                            @endif
                            >
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-4">
                            <label for="hourmeter" class="form-label">Hourmeter (jam)</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" id="hourmeter" class="form-control" name="f_hourmeter"
                            @if ($operationActive->s_hourmeter)
                                required
                            @else 
                                @readonly(true)
                            @endif
                            >
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-4">
                            <label for="bahan-bakar" class="form-label">Bahan Bakar (Liter)</label>
                        </div>
                        <div class="col-md-8">
                            <input type="number" id="bahan-bakar" class="form-control" name="fuel_inflow">
                        </div>
                    </div>                    
                    <div class="row mb-3">
                        <div class="col-md-4">
                            <label for="perjalanan" class="form-label">Perjalanan (KM)</label>
                        </div>
                        <div class="col-md-8">
                            <input type="number" id="perjalanan" class="form-control" name="trip">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-5">
                            <label for="perjalanan" class="form-label"></label>
                        </div>
                        <div class="col-md-7">
                            <label for="perjalanan" class="form-label">Waktu kendala (opsional)</label>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-4">
                            <label for="kendala-mulai" class="form-label">Mulai Kendala</label>
                        </div>
                        <div class="col-md-8">
                            <input type="time" id="kendala-mulai" class="form-control" name="s_otherdelay">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-md-4">
                            <label for="kendala-selesai" class="form-label">Selesai kendala</label>
                        </div>
                        <div class="col-md-8">
                            <input type="time" id="kendala-selesai" class="form-control" name="f_otherdelay">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-info" data-bs-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Selesai</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        const odometerInput = document.getElementById("odometer");

        odometerInput.addEventListener("input", function () {
            const inputValue = parseFloat(odometerInput.value);
            const sOdometerValue = parseFloat("{{ $operationActive->s_odometer }}");

            if (!isNaN(inputValue) && !isNaN(sOdometerValue) && inputValue < sOdometerValue) {
                odometerInput.setCustomValidity("Nilai harus lebih besar dari " + sOdometerValue);
            } else {
                odometerInput.setCustomValidity("");
            }
        });
    });

    document.addEventListener("DOMContentLoaded", function () {
        const hourmeterInput = document.getElementById("hourmeter");

        hourmeterInput.addEventListener("input", function () {
            const inputValue = parseFloat(hourmeterInput.value);
            const sHourmeterValue = parseFloat("{{ $operationActive->s_hourmeter }}");

            if (!isNaN(inputValue) && !isNaN(sHourmeterValue) && inputValue < sHourmeterValue) {
                hourmeterInput.setCustomValidity("Nilai harus lebih besar dari " + sHourmeterValue);
            } else {
                hourmeterInput.setCustomValidity("");
            }
        });
    });
</script>