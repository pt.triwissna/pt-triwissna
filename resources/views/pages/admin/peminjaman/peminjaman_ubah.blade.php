@extends('app')
@section('title-app')
    Tambah Peminjaman Aktif
@endsection
@section('navbar-title-back')
    Peminjaman Aktif
@endsection
@section('navbar-title-target')
    / Ubah
@endsection
@section('link-back')
    {{ route('peminjaman-aktif') }}
@endsection


@section('content')
<div class="card p-3">
        <div class="container-view">
            <a href="/peminjaman">
                <button type="button" class="btn btn-primary">Kembali</button>
            </a>
            <hr>
            <br>
            <form action="{{ route('update', ['id' => $operationActive->id_operation_active]) }}" method="post">
                @csrf
                @method('PUT')
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Tanggal</label>
                    <div class="col-sm-6">
                        <input type="date" class="form-control @error('date') is-invalid @enderror"
                            id="basic-default-name" name="date" value="{{ old('date', $operationActive->date) }}" />
                        @error('date')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Shift</label>
                    <div class="col-sm-6">
                        <select name="shift" class="form-select @error('shift') is-invalid @enderror" id="exampleFormControlSelect1"
                            aria-label="Default select example" >
                            <option></option>
                            <option value="DAY" {{$operationActive->shift || old('shift') == 'DAY' ? 'selected' : '' }}>DAY</option>
                            <option value="NIGHT" {{$operationActive->shift || old('shift') == 'NIGHT' ? 'selected' : '' }}>NIGHT</option>
                        </select>
                        @error('shift')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Jam Mulai</label>
                    <div class="col-sm-6">
                        <input type="time" name="s_work_time"
                            class="form-control @error('s_work_time') is-invalid @enderror" id="basic-default-name"
                            value="{{ old('date', $operationActive->s_work_time) }}" />
                        @error('s_work_time')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Proyek</label>
                    <div class="col-sm-6">
                        <input type="text" name="project" class="form-control @error('project') is-invalid @enderror"
                            id="basic-default-name" value="{{ old('project', $operationActive->project) }}" />
                        @error('project')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Operator</label>
                    <div class="col-sm-6">
                        <input type="text" name="operator" class="form-control @error('operator') is-invalid @enderror"
                            id="basic-default-name" value="{{ old('operator', $operationActive->operator) }}" />
                        @error('operator')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Unit</label>
                    <div class="col-sm-6">
                        <select name="asset_id" class="form-select" id="exampleFormControlSelect1"
                            aria-label="Default select example">
                            <option value="{{ $getAsset->asset_id }}">{{ $getAsset->no_unit }} - {{ $getAsset->manufacture }}</option>
                        </select>
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Odometer</label>
                    <div class="col-sm-6">
                        <input type="number" name="s_odometer"
                            class="form-control @error('s_odometer') is-invalid @enderror" id="basic-default-name"
                            value="{{ old('s_odometer', $operationActive->s_odometer) }}" />
                        @error('s_odometer')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Hour Meter</label>
                    <div class="col-sm-6">
                        <input type="number" name="s_hourmeter"
                            class="form-control @error('s_hourmeter') is-invalid @enderror" id="basic-default-name"
                            value="{{ old('s_hourmeter', $operationActive->s_hourmeter) }}" />
                        @error('s_hourmeter')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row justify-content-end">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            var dateInput = document.getElementById("date");
            var pickDateButton = document.getElementById("pickDateButton");

            pickDateButton.addEventListener("click", function() {
                dateInput.click();
            });

            dateInput.addEventListener("change", function() {
                if (this.value) {
                    var formattedDate = formatDateToDMY(this.value);
                    this.value = formattedDate;
                }
            });
        });
    </script>
@endsection
