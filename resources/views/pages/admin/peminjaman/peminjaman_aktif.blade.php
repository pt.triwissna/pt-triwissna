@extends('app')
@section('title-app')
    Peminjaman Aktif
@endsection
@section('navbar-title-back')
    Peminjaman Aktif
@endsection
@section('link-back')
    {{ route('peminjaman-aktif') }}
@endsection
@section('content')

    @if (session('success'))
        @include('partials.alert-success')
    @endif

    @if (session('error'))
        @include('partials.alert-error')
    @endif
    <div class="card p-3">
        <div class="d-grid gap-2 d-md-flex justify-content-md-end">
            @if ($loggedInUser->role->role == 'admin')
                <a href="peminjaman/tambah/data" class="btn btn-primary me-md-2 pe-5 ps-5">Tambah</a>
            @endif
        </div>
        <form action="{{ route('peminjaman-aktif') }}" method="get">
            @csrf
            @if ($loggedInUser->role->role != 'admin')
                <div class="row mb-5 mt-5">
                @else
                    <div class="row mb-5">
            @endif
            <div class="col-md-3">
                <label for="" class="fw-bold">Nomor Unit</label>
                <input name="no_unit" type="text" class="form-control" placeholder="Nomor Unit"
                    value="{{ isset($_GET['no_unit']) ? $_GET['no_unit'] : '' }}">
            </div>
            <div class="col-md-3">
                <label for="" class="fw-bold">Shift</label>
                <select name="shift" class="form-select">
                    <option value="">- Semua -</option>
                    <option value="DAY" {{ isset($_GET['shift']) && $_GET['shift'] == 'DAY' ? 'selected' : '' }}>DAY
                    </option>
                    <option value="NIGHT" {{ isset($_GET['shift']) && $_GET['shift'] == 'NIGHT' ? 'selected' : '' }}>NIGHT
                    </option>
                </select>
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-primary mt-4">Cari</button>
            </div>
    </div>
    </form>

    <div class="table-responsive text-nowrap">
        <table class="table table-striped table-hover">
            <thead>
                <tr class="table-active">
                    <th class="fw-bold">Aksi</th>
                    <th class="fw-bold">Shift</th>
                    <th class="fw-bold">No. Unit</th>
                    <th class="fw-bold">Merek</th>
                    <th class="fw-bold">Tipe</th>
                    <th class="fw-bold">Kategori</th>
                    <th class="fw-bold">Jam</th>
                    <th class="fw-bold">Operator</th>
                </tr>
            </thead>
            <tbody class="table-border-bottom-0">
                @if (count($OperationActive) < 1)
                    <tr>
                        <td colspan="8" style="padding: 20px; font-size: 20px;"><span>Tidak ditemukan data</span>
                        </td>
                    </tr>
                @else
                    @foreach ($OperationActive as $operationActive)
                        @include('pages.admin.peminjaman.popup.popup-detail-peminjaman')
                        @include('pages.admin.peminjaman.popup.popup-revisi')
                        @include('pages.admin.peminjaman.popup.popup-selesai')

                        <tr>
                            <td>
                                <button class="btn btn-primary btn-sm" type="button" data-bs-toggle="modal"
                                    data-bs-target="#detailModalPeminjaman{{ $operationActive->id_operation_active }}">DETAIL</button>
                                @if ($loggedInUser->role->role == 'admin' || $loggedInUser->role->role == 'supervisor')
                                    @if ($operationActive->flg_action == 'H' or $operationActive->flg_action == null)
                                        <button class="btn btn-danger btn-sm" type="button" data-bs-toggle="modal"
                                            data-bs-target="#revisi{{ $operationActive->id_operation_active }}">REVISI</button>
                                    @else
                                        <button class="btn btn-danger btn-sm" type="button" data-bs-toggle="modal"
                                            data-bs-target="#revisi{{ $operationActive->id_operation_active }}"
                                            disabled>REVISI</button>
                                    @endif
                                @endif
                                @if ($loggedInUser->role->role == 'admin')
                                    @if ($operationActive->flg_action == 'H' || $operationActive->flg_action == null)
                                        <button class="btn btn-success btn-sm" type="button" data-bs-toggle="modal"
                                            data-bs-target="#selesai{{ $operationActive->id_operation_active }}">SELESAI</button>
                                    @else
                                        <button class="btn btn-success btn-sm" type="button" data-bs-toggle="modal"
                                            data-bs-target="#selesai{{ $operationActive->id_operation_active }}"
                                            disabled>SELESAI</button>
                                    @endif
                                @endif
                                @if ($loggedInUser->role->role == 'supervisor')
                                    <a href="{{ route('edit-peminjaman', ['id' => $operationActive->id_operation_active, 'asset_id' => $operationActive->asset_id]) }}"
                                        class="btn btn-warning btn-sm">UBAH</a>
                                @endif
                            </td>
                            <td>{{ $operationActive->shift }}</td>
                            <td>{{ $operationActive->no_unit }}</td>
                            <td>{{ $operationActive->manufacture }}</td>
                            <td>{{ $operationActive->type_name }}</td>
                            <td>{{ $operationActive->ctgr_name }}</td>
                            <td>{{ $operationActive->s_work_time }}</td>
                            <td>{{ $operationActive->operator }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="row pt-5">
        <div class="col-lg-10">

            <br>
            <span>Total Peminjaman Aktif {{ $total[0]->totalData }}</span>

        </div>
    </div>
    </div>
@endsection
