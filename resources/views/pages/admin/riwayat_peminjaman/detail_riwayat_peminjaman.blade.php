@php
    // Import class Carbon
    use Carbon\Carbon;

    // Set locale ke bahasa Indonesia
    Carbon::setLocale('id');

    // Mendapatkan nama hari dalam bahasa Indonesia
    $dayInIndonesian = Carbon::parse($operationshistory->date)->translatedFormat('l');
    $monthsInIndonesian = [
        1 => 'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember',
    ];

    // Ubah format tampilan tanggal
    $date = Carbon::createFromFormat('Y-m-d', $operationshistory->date); // Ganti dengan tanggal yang sesuai
    $formattedDate = $date->format('d') . ' ' . $monthsInIndonesian[$date->format('n')] . ' ' . $date->format('Y');
@endphp


<div class="modal fade" id="detailModalRiwayat{{ $operationshistory->id_operation }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel3">Detail Riwayat Peminjaman</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="card-btn d-flex justify-content-end">
                <a href="{{ route('cetakPinjaman.pdf', ['id' => $operationshistory->id_operation]) }}" class="btn btn-primary" style="margin-right: 25px;">Cetak PDF</a>
            </div>
            <hr>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label" for="basic-default-name">Hari / Tanggal</label>
                            <div class="col-sm-9">
                                <input disabled type="text" class="form-control" id="basic-default-name" value="{{ $dayInIndonesian }}, {{ $formattedDate ?? '' }}"/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label" for="basic-default-company">Shift</label>
                            <div class="col-sm-9">
                                <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $operationshistory->shift }}"/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label" for="basic-default-company">Jam</label>
                            <div class="col-sm-9">
                                <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $operationshistory->s_work_time }} - {{ $operationshistory->f_work_time }}"/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label" for="basic-default-company">Kendala</label>
                            <div class="col-sm-9">
                                <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $operationshistory->s_otherdelay }} - {{ $operationshistory->f_otherdelay }}"/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label" for="basic-default-company">Proyek</label>
                            <div class="col-sm-9">
                                <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $operationshistory->project }}"/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label" for="basic-default-company">Operator</label>
                            <div class="col-sm-9">
                                <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $operationshistory->operator }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label" for="basic-default-name">No. Unit</label>
                            <div class="col-sm-9">
                                <input disabled type="text" class="form-control" id="basic-default-name" value="{{ $operationshistory->no_unit }}"/>
                            </div> 
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label" for="basic-default-company">Tipe</label>
                            <div class="col-sm-9">
                                <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $operationshistory->type_name }}"/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label" for="basic-default-company">Kategori</label>
                            <div class="col-sm-9">
                                <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $operationshistory->ctgr_name }}"/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label" for="basic-default-company">Merek</label>
                            <div class="col-sm-9">
                                <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $operationshistory->manufacture }}"/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label" for="basic-default-company">Model</label>
                            <div class="col-sm-9">
                                <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $operationshistory->model }}"/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label" for="basic-default-company">Tahun</label>
                            <div class="col-sm-9">
                                <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $operationshistory->yom }}"/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label" for="basic-default-company">Hour Meter</label>
                            <div class="col-sm-9">
                                <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $operationshistory->s_hourmeter }} - {{ $operationshistory->f_hourmeter }}"/>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label" for="basic-default-company">Odometer</label>
                            <div class="col-sm-9">
                                <input disabled type="text" class="form-control" id="basic-default-company" value="{{ $operationshistory->s_odometer }} - {{ $operationshistory->f_odometer }}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>