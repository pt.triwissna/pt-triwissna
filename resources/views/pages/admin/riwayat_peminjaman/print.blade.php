<!DOCTYPE html>
<html>
<head>
    <title>Detail Riwayat Peminjaman</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }

        h1 {
            font-size: 24px;
            margin-bottom: 20px;
        }

        h3 {
            font-size: 18px;
            margin-top: 10px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
        }

        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #f2f2f2;
        }

        hr {
            border: 1px solid #ccc;
            margin: 20px 0;
        }
    </style>
</head>
<body>
    <h1>Detail Riwayat Peminjaman</h1>
    <hr>
    <table>
        <tr>
            <th>Hari / Tanggal</th>
            <td>
                {{ \Carbon\Carbon::parse($operationshistory->date)->locale('id')->isoFormat('dddd, DD MMMM YYYY') }}
            </td>
        </tr>                    
        <tr>
            <th>Shift</th>
            <td>{{ $operationshistory->shift }}</td>
        </tr>
        <tr>
            <th>Jam</th>
            <td>{{ $operationshistory->s_work_time }} - {{ $operationshistory->f_work_time }}</td>
        </tr>
        <tr>
            <th>Kendala</th>
            <td>{{ $operationshistory->s_otherdelay }} - {{ $operationshistory->f_otherdelay }}</td>
        </tr>
        <tr>
            <th>Proyek</th>
            <td>{{ $operationshistory->project }}</td>
        </tr>
        <tr>
            <th>Operator</th>
            <td>{{ $operationshistory->operator }}</td>
        </tr>
        <tr>
            <th>No. Unit</th>
            <td>{{ $operationshistory->asset->no_unit }}</td>
        </tr>
        <tr>
            <th>Tipe</th>
            <td>{{ $typeAsset->type_name }}</td>
        </tr>
        <tr>
            <th>Kategori</th>
            <td>{{ $typeCtgr->ctgr_name }}</td>
        </tr>
        <tr>
            <th>Merek</th>
            <td>{{ $operationshistory->asset->manufacture }}</td>
        </tr>
        <tr>
            <th>Model</th>
            <td>{{ $operationshistory->asset->model }}</td>
        </tr>
        <tr>
            <th>Tahun</th>
            <td>{{ $operationshistory->asset->yom }}</td>
        </tr>
        <tr>
            <th>Hour Meter</th>
            <td>{{ $operationshistory->s_hourmeter }} - {{ $operationshistory->f_hourmeter }}</td>
        </tr>
        <tr>
            <th>Odometer</th>
            <td>{{ $operationshistory->s_odometer }} - {{ $operationshistory->f_odometer }}</td>
        </tr>
    </table>
</body>
</html>
