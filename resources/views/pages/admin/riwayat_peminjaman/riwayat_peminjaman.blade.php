@extends('app')
@section('title-app')
    Riwayat Peminjaman
@endsection
@section('navbar-title-back')
    Riwayat Peminjaman
@endsection
@section('content')
    <div class="card p-3">
        <form action="{{ route('riwayat-peminjaman') }}" method="GET">
            <div class="row mt-4 mb-5">
                <div class="col-md-3">
                    <div>
                        <label for="filter" class="fw-bold">Nomor Unit</label>
                        <input type="text" class="form-control" id="defaultFormControlInput" placeholder="Nomor Unit"
                            aria-describedby="defaultFormControlHelp" name="no_unit"
                            value="{{ isset($_GET['no_unit']) ? $_GET['no_unit'] : '' }}" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div>
                        <label for="date" class="fw-bold">Tanggal</label>
                        <input type="date" class="form-control" id="defaultFormControlInput"
                            aria-describedby="defaultFormControlHelp" name="date"
                            value="{{ isset($_GET['date']) ? $_GET['date'] : '' }}" />
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="" class="fw-bold">Shift</label>
                    <select name="shift" class="form-select">
                        <option value="">-  Semua  -</option>
                        <option value="DAY" {{ isset($_GET['shift']) && $_GET['shift'] == 'DAY' ? 'selected' : '' }}>DAY
                        </option>
                        <option value="NIGHT" {{ isset($_GET['shift']) && $_GET['shift'] == 'NIGHT' ? 'selected' : '' }}>
                            NIGHT</option>
                    </select>
                </div>
                <div class="col-md-1  d-flex align-items-end">
                    <button type="submit" class="btn btn-primary">Cari</button>
                </div>
            </div>
        </form>
        <div class="table-responsive text-nowrap">
            <table class="table table-striped table-hover">
                <thead>
                    <tr class="table-active">
                        <th class="fw-bold">Aksi</th>
                        <th class="fw-bold">Tanggal</th>
                        <th class="fw-bold">Shift</th>
                        <th class="fw-bold">No. Unit</th>
                        <th class="fw-bold">Tipe</th>
                        <th class="fw-bold">Kategori</th>
                        <th class="fw-bold">Jam</th>
                        <th class="fw-bold">Operator</th>
                    </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                    @if (count($OperationsHistory) < 1)
                        <tr>
                            <td colspan="8" style="padding: 20px; font-size: 20px;"><span>Tidak ditemukan data</span>
                            </td>
                        </tr>
                    @else
                        @foreach ($OperationsHistory as $operationshistory)
                        @include('pages.admin.riwayat_peminjaman.detail_riwayat_peminjaman')
                            <tr>
                                <td>
                                    <button class="btn btn-primary btn-sm" type="button" data-bs-toggle="modal"
                                        data-bs-target="#detailModalRiwayat{{ $operationshistory->id_operation }}">DETAIL</button>
                                </td>
                                <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $operationshistory->date)->format('d-m-Y') }}
                                </td>
                                <td>{{ $operationshistory->shift }}</td>
                                <td>{{ $operationshistory->no_unit }}</td>
                                <td>{{ $operationshistory->type_name }}</td>
                                <td>{{ $operationshistory->ctgr_name }}</td>
                                <td>{{ $operationshistory->s_work_time }} - {{ $operationshistory->f_work_time }}</td>
                                <td>{{ $operationshistory->operator }}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
        <div class="row pt-5">
            <div class="col-lg-10">
                <ul class="pagination">
                    {{ $OperationsHistory->links() }}
                </ul>
                <br>
                <span>Total data {{ $totalData[0]->total_count }}, halaman {{ $OperationsHistory->currentPage() }} dari
                    {{ $OperationsHistory->lastPage() }}</span>

            </div>
        </div>
    </div>

@endsection
