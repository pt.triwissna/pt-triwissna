@extends('pages.admin.message_adm.index_message')
@section('content')
@section('title-app')
    Permimtaan
@endsection
@section('navbar-title')
    Permintaan
@endsection
<style>
/* Style CSS untuk list data yang lebih elegan */
.list-item {
    border: 1px solid #e0e0e0;
    margin-bottom: 20px;
    padding: 20px;
    background-color: #fff;
    border-radius: 5px;
    box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
    transition: box-shadow 0.3s ease;
}

.list-item:hover {
    box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2);
}

.item-content {
    display: flex;
    justify-content: space-between;
    align-items: center;
}

.date-text {
    font-size: 18px;
    font-weight: 600;
    color: #333;
}

</style>
<div class="card p-3">
    <div class="table-responsive text-nowrap" id="permintaan-peminjaman-list">
        <!-- Tabel akan diperbarui melalui AJAX -->
    </div>
</div>

<script src="{{ asset('js/pesan_admn/permintaan.js') }}"></script>
<script>
    const loggedInUser = @json($loggedInUser);
</script>
@endsection
