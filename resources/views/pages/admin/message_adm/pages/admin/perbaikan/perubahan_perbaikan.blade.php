@extends('pages.admin.message_adm.index_message')
@section('title-app')
    Perubahan Perbaikan
@endsection
@section('navbar-title')
    Perubahan Perbaikan
@endsection
@section('content')
<style>
    #maintenance-messages-table {
    overflow: hidden; /* Menghilangkan scrollbar vertikal */
}
</style>
<div class="card p-3">
    <div class="table-responsive text-nowrap" id="maintenance-messages-table">
        <!-- Tabel akan diperbarui melalui AJAX -->
    </div>
</div>
<script src="{{ asset('js/pesan_admn/perbaikan/perubahan_perbaikan.js') }}"></script>
<script>
    const loggedInUser = @json($loggedInUser);
</script>
@endsection