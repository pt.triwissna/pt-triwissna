@extends('pages.admin.message_adm.index_message')
@section('title-app')
    Riwayat Perubahan Perbaikan
@endsection
@section('navbar-title')
    Riwayat Perubahan Perbaikan
@endsection
@section('content')
    <div class="card p-3">
        <form method="GET" action="{{ route('riwayatPerubahanPerbaikan') }}">
            @csrf
            <div class="row mb-3">

                <div class="col-1"><label for="filter" class="fw-bold">Tanggal</label></div>
                <div class="col-3">
                    <input name="date_message" type="date" class="form-control" id="defaultFormControlInput"
                        aria-describedby="defaultFormControlHelp"
                        value="{{ isset($_GET['date_message']) ? $_GET['date_message'] : '' }}" />
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-primary">Cari</button>
                </div>

            </div>
        </form>
        <div class="table-responsive text-nowrap">


            <div class="accordion mt-3" id="accordionExample">
                @foreach ($riwayatPerbaikan as $item)
                    <div class="card accordion-item">
                        <h2 class="accordion-header">
                            <button type="button" class="accordion-button" data-bs-toggle="collapse"
                                data-bs-target="#accordionOne{{ $item->id_maintenance_message }}" aria-expanded="true"
                                aria-controls="accordionOne">
                                <span class="fw-bold text-danger">{{ $item->date_message }} </span> <span class="fs-5"> :
                                    {{ $item->title_message }}</span>
                            </button>
                        </h2>

                        <div id="accordionOne{{ $item->id_maintenance_message }}" class="accordion-collapse collapse"
                            data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <span class="fw-bold">Detail:</span>
                                {{ $item->contents_message }}

                                <span class="fw-bold">Catatan :</span> {{ $item->pesan }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>



    {{-- <script src="{{ asset('js/pesan_admn/perbaikan/riwayat_perubahan_perbaikan.js') }}"></script> --}}
    {{-- <script>
    const loggedInUser = @json($loggedInUser);
</script> --}}
@endsection
