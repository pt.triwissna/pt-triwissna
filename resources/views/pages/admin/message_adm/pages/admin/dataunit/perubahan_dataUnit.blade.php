@extends('pages.admin.message_adm.index_message')
@section('title-app')
    Perubahan Data Unit
@endsection
@section('navbar-title')
    Perubahan Data Unit
@endsection
<style>
    #operations-messages-table {
    overflow: hidden; /* Menghilangkan scrollbar vertikal */
}
</style>
@section('content')
<div class="card p-3">
    <div class="table-responsive text-nowrap" id="dataUnit-messages-table">
        <!-- Tabel akan diperbarui melalui AJAX -->
    </div>
</div>
<script src="{{ asset('js/pesan_admn/dataUnit/perubahan_dataUnit.js') }}"></script>
<script>
    const loggedInUser = @json($loggedInUser);
</script>
@endsection


