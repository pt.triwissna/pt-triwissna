@extends('pages.admin.message_adm.index_message')


@section('title-app')
    Edit Data Unit
@endsection
@section('link-back')
    {{ route('data-unit') }}
@endsection
@section('content')
    <div class="card p-3">
        <div class="container-view">
            <div>
                <a href="{{ route('pesan-data-unit') }}" type="button" class="btn btn-primary fw-bold"> <i class='menu-icon bx bx-left-arrow-alt fw-bold' ></i></a>
            </div>
            <br>
            <form action="{{ route('update_pesanDataUnit', ['id' => $dataUnit->asset_id, 'id_message' => $id_asset]) }}" method="post">
                @csrf
                @method('PUT')
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Tipe</label>
                    <div class="col-sm-6">
                        <select name="id_type" class="form-select {{ $errors->has('id_type') ? 'is-invalid' : '' }}"
                            id="exampleFormControlSelect1" aria-label="Default select example">
                            <option></option>
                            @foreach ($typeAsset as $type)
                                <option value="{{ $type->id_type }}"
                                    {{ old('id_type') == $type->id_type || $type->id_type == $dataUnit->id_type ? 'selected' : '' }}>
                                    {{ $type->type_name }}</option>
                            @endforeach
                        </select>
                        @error('id_type')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Kategori</label>
                    <div class="col-sm-6">
                        <select name="id_ctgr" class="form-select {{ $errors->has('id_ctgr') ? 'is-invalid' : '' }}"
                            id="exampleFormControlSelect2" aria-label="Default select example">
                            <option></option>
                            @foreach ($category as $ctgr)
                                <option value="{{ $ctgr->id_ctgr }}"
                                    {{ old('id_ctgr') == $ctgr->id_ctgr || $ctgr->id_ctgr == $dataUnit->id_ctgr ? 'selected' : '' }}>
                                    {{ $ctgr->ctgr_name }}
                                </option>
                            @endforeach
                        </select>
                        @error('id_ctgr')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Merek</label>
                    <div class="col-sm-6">
                        <input type="text" name="manufacture"
                            class="form-control @error('manufacture') is-invalid @enderror"
                            value="{{ old('manufacture') }}{{ $dataUnit->manufacture }}" id="basic-default-name" />
                        @error('manufacture')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Model</label>
                    <div class="col-sm-6">
                        <input type="text" name="model" class="form-control @error('model') is-invalid @enderror"
                            value="{{ old('model') }}{{ $dataUnit->model }}" id="basic-default-name" />
                        @error('model')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Tahun</label>
                    <div class="col-sm-6">
                        <input type="number" name="yom" class="form-control @error('yom') is-invalid @enderror"
                            value="{{ old('yom') }}{{ $dataUnit->yom }}" id="basic-default-name" placeholder="yyyy" />
                        @error('yom')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">No. Unit</label>
                    <div class="col-sm-6">
                        <input type="text" name="no_unit" class="form-control @error('no_unit') is-invalid @enderror"
                            value="{{ old('no_unit') }}{{ $dataUnit->no_unit }}" id="basic-default-name" />
                        @error('no_unit')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>
            
                
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Tipe Asuransi</label>
                    <div class="col-sm-6">
                        <select name="id_insurance_type" class="form-select" id="exampleFormControlSelect3"
                            aria-label="Default select example">
                            <option></option>
                            @foreach ($typeInsurance as $insurance)
                                <option value="{{ $insurance->id_insurance_type }}"
                                    {{ old('id_insurance_type') == $insurance->id_insurance_type || $insurance->id_insurance_type == $dataPermitsInsurance->id_insurance_type ? 'selected=' : '' }}
                                    >{{ $insurance->insurance_type_name }}
                                </option>
                                @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Tahun Asuransi</label>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-5">
                                <input type="number" name="insurance_issued" value="{{ $dataPermitsInsurance->insurance_issued }}" class="form-control" id="basic-default-name"
                                    maxlength="4" placeholder="yyyy" />
                            </div>
                            <div class="col-1 text-center">
                                <span class="fw-bold fs-4">-</span>
                            </div>
                            <div class="col-5">
                                <input type="number" name="insurance_expired" value="{{ $dataPermitsInsurance->insurance_expired }}" class="form-control" id="basic-default-name"
                                    maxlength="4" placeholder="yyyy" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Tahun STNK</label>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-5">
                                <input type="number" name="stnk_issued" value="{{ $dataPermitsInsurance->stnk_issued }}" class="form-control" id="basic-default-name"
                                    maxlength="4" placeholder="yyyy" />
                            </div>
                            <div class="col-1 text-center">
                                <span class="fw-bold fs-4">-</span>
                            </div>
                            <div class="col-5">
                                <input type="number" name="stnk_expired" value="{{ $dataPermitsInsurance->stnk_expired }}" class="form-control" id="basic-default-name"
                                    maxlength="4" placeholder="yyyy" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Tahun KIR</label>
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-5">
                                <input type="number" name="kir_issued" value="{{ $dataPermitsInsurance->kir_issued }}" class="form-control" id="basic-default-name"
                                    maxlength="4" placeholder="yyyy" />
                            </div>
                            <div class="col-1 text-center">
                                <span class="fw-bold fs-4">-</span>
                            </div>
                            <div class="col-5">
                                <input type="number" name="kir_expired" value="{{ $dataPermitsInsurance->kir_expired }}" class="form-control" id="basic-default-name"
                                    maxlength="4" placeholder="yyyy" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-3 justify-content-start mb-3">
                    <label class="col-sm-2 col-form-label text-center" for="basic-default-name">Pesan</label>
                    <div class="col-sm-6">
                        <input type="text" name="pesan" class="form-control @error('pesan') is-invalid @enderror"
                            value="{{ old('pesan') }}{{ $dataUnit->pesan }}" id="basic-default-name" />
                        @error('pesan')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                </div>

                <div class="row justify-content-end mt-2 mb-5">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script>
        // Dapatkan elemen-elemen yang dibutuhkan
        const tipeAsuransiInput = document.querySelector('#exampleFormControlSelect3');
        const tahunAsuransiInput = document.querySelector('input[name="insurance_issued"]');
        const tahunSTNKInput = document.querySelector('input[name="stnk_issued"]');
        const tahunKIRInput = document.querySelector('input[name="kir_issued"]');
        const tahunExperiedAsuransi = document.querySelector('input[name="insurance_expired"]');
        const tahunExperiedSTNK = document.querySelector('input[name="stnk_expired"]');
        const tahunExperiedKIR = document.querySelector('input[name="kir_expired"]');


        // Tambahkan event listener untuk input Tipe Asuransi
        tipeAsuransiInput.addEventListener('change', function() {
            // Periksa apakah Tipe Asuransi sudah dipilih
            if (tipeAsuransiInput.value !== '') {
                // Jika sudah dipilih, tambahkan atribut "required" ke input Tahun Asuransi, Tahun STNK, dan Tahun KIR
                tahunAsuransiInput.setAttribute('required', 'required');
                tahunSTNKInput.setAttribute('required', 'required');
                tahunKIRInput.setAttribute('required', 'required');
                tahunExperiedAsuransi.setAttribute('required', 'required');
                tahunExperiedSTNK.setAttribute('required', 'required');
                tahunExperiedKIR.setAttribute('required', 'required');
            } else {
                // Jika belum dipilih, hapus atribut "required" dari input Tahun Asuransi, Tahun STNK, dan Tahun KIR
                tahunAsuransiInput.removeAttribute('required');
                tahunSTNKInput.removeAttribute('required');
                tahunKIRInput.removeAttribute('required');
                tahunExperiedAsuransi.removeAttribute('required');
                tahunExperiedSTNK.removeAttribute('required');
                tahunExperiedKIR.removeAttribute('required');
            }
        });
    </script>
@endsection
