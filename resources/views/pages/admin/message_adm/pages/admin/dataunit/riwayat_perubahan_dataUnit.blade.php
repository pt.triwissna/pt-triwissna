@extends('pages.admin.message_adm.index_message')
@section('title-app')
    Riwayat Perubahan Data Unit
@endsection
@section('navbar-title')
    Riwayat Perubahan Data Unit
@endsection
@section('content')
    {{-- <div class="card p-3">
    <div class="table-responsive text-nowrap" id="riwayat-perubahan-dataUnit-table">
        
    </div>
</div>

<script src="{{ asset('js/pesan_admn/dataunit/riwayat_perubahan_dataUnit.js') }}"></script>
<script>
    const loggedInUser = @json($loggedInUser);
</script> --}}

    <div class="card p-3">
        <form method="GET" action="{{ route('riwayatPerubahanDataUnit') }}">
            @csrf
            <div class="row mb-3">

                <div class="col-1"><label for="filter" class="fw-bold">Tanggal</label></div>
                <div class="col-3">
                    <input name="date_message" type="date" class="form-control" id="defaultFormControlInput"
                        aria-describedby="defaultFormControlHelp"
                        value="{{ isset($_GET['date_message']) ? $_GET['date_message'] : '' }}" />
                </div>
                <div class="col-1">
                    <button type="submit" class="btn btn-primary">Cari</button>
                </div>

            </div>
        </form>
        <div class="table-responsive text-nowrap">


            <div class="accordion mt-3" id="accordionExample">
                @foreach ($riwayatDataUnit as $item)
                    <div class="card accordion-item">
                        <h2 class="accordion-header">
                            <button type="button" class="accordion-button" data-bs-toggle="collapse"
                                data-bs-target="#accordionOne{{ $item->id_dataUnit_message }}" aria-expanded="true"
                                aria-controls="accordionOne">
                                <span class="fw-bold text-danger">{{ $item->date_message }} </span> <span class="fs-5"> :
                                    {{ $item->title_message }}</span>
                            </button>
                        </h2>

                        <div id="accordionOne{{ $item->id_dataUnit_message }}" class="accordion-collapse collapse"
                            data-bs-parent="#accordionExample">
                            <div class="accordion-body">
                                <span class="fw-bold">Detail:</span>
                                {{ $item->contents_message }}

                                <span class="fw-bold">Catatan :</span> {{ $item->pesan }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
