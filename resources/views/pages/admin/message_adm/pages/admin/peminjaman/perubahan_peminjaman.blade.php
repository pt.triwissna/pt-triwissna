@extends('pages.admin.message_adm.index_message')
@section('title-app')
    Perubahan Peminjaman
@endsection
@section('navbar-title')
    Perubahan Peminjaman
@endsection
<style>
    #operations-messages-table {
    overflow: hidden; /* Menghilangkan scrollbar vertikal */
}
</style>
@section('content')
<div class="card p-3">
    <div class="table-responsive text-nowrap" id="operations-messages-table">
        <!-- Tabel akan diperbarui melalui AJAX -->
    </div>
</div>
<script src="{{ asset('js/pesan_admn/peminjaman/perubahan_peminjaman.js') }}"></script>
<script>
    const loggedInUser = @json($loggedInUser);
</script>
@endsection



{{-- id_operations_message --}}