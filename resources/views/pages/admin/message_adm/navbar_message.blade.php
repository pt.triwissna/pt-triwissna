<nav class="navbar navbar-expand-xl navbar-detached align-items-center bg-navbar-theme" id="layout-navbar">
    <div class="navbar-nav-right d-flex align-items-center" id="navbar-collapse">
        <div class="navbar-nav align-items-center">
            <div class="nav-item d-flex align-items-center pt-2">
                <span class="fs-4 me-2 ms-3 text-center fw-bold">@yield('navbar-title')</span>
            </div>
        </div>
    </div>
</nav>
