<style>
    /* Menambahkan border pada elemen yang memiliki class .active-2 */
    .active-2 {
        background-color: #acdae4;
        border-radius: 10px;
        margin: 10px;
    }
</style>
<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand-demo">
        <div class=" d-flex justify-content-start ps-3 pe-5">
            <a href="/" type="button" class="btn btn-primary ps-3 pe-3 fw-bold"> <i
                    class='menu-icon bx bx-left-arrow-alt fw-bold'></i>Kembali</a>
        </div>
        {{-- class logo --}}
        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>
    <div class="menu-inner-shadow"></div>
    <ul class="menu-inner py2">
        @if ($loggedInUser->role->role == 'admin')
            <!-- Peminjaman -->
            <li
                class="menu-item {{ request()->routeIs('pesan-perubahan-peminjaman', 'riwayatPerubahanPeminjaman', 'editOperationShow') ? 'active open' : '' }}">
                <a href="javascript:void(0);" class="menu-link menu-toggle">
                    <i class='menu-icon bx bx-message-alt-edit'></i>
                    <div data-i18n="peminjaman">Peminjaman</div>
                </a>
                <ul class="menu-sub">
                    <li
                        class="menu-item {{ request()->routeIs('pesan-perubahan-peminjaman', 'editOperationShow') ? 'active' : (request()->routeIs('edit-operation') ? 'active-2' : '') }}">
                        <a href="{{ route('pesan-perubahan-peminjaman') }}" class="menu-link">
                            <div data-i18n="peminjaman-aktif">Perubahan Peminjaman</div>
                        </a>
                    </li>
                    <li class="menu-item {{ request()->routeIs('riwayatPerubahanPeminjaman') ? 'active' : '' }}">
                        <a href="{{ route('riwayatPerubahanPeminjaman') }}" class="menu-link">
                            <div data-i18n="riwayat-peminjaman">Riwayat Perubahan Peminjaman</div>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Perbaikan -->
            <li
                class="menu-item {{ request()->routeIs('pesan-perubahan-perbaikan', 'riwayatPerubahanPerbaikan', 'edit-maintenance') ? 'active open' : '' }}">
                <a href="javascript:void(0);" class="menu-link menu-toggle">
                    <i class='menu-icon bx bx-message-alt-edit'></i>
                    <div data-i18n="perbaikan">Perbaikan</div>
                </a>
                <ul class="menu-sub">
                    <li class="menu-item {{ request()->routeIs('pesan-perubahan-perbaikan', 'edit-maintenance') ? 'active' : '' }}">
                        <a href="{{ route('pesan-perubahan-perbaikan') }}" class="menu-link">
                            <div data-i18n="perbaikan-aktif">Perubahan Perbaikan</div>
                        </a>
                    </li>
                    <li class="menu-item {{ request()->routeIs('riwayatPerubahanPerbaikan') ? 'active' : '' }}">
                        <a href="{{ route('riwayatPerubahanPerbaikan') }}" class="menu-link">
                            <div data-i18n="riwayat-perbaikan">Riwayat Perubahan Perbaikan</div>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Data Unit -->
            <li
                class="menu-item {{ request()->routeIs('pesan-data-unit', 'riwayatPerubahanDataUnit', 'edit-data-unit') ? 'active open' : '' }}">
                <a href="javascript:void(0);" class="menu-link menu-toggle">
                    <i class='menu-icon bx bx-message-alt-edit'></i>
                    <div data-i18n="data_unit">Data Unit</div>
                </a>
                <ul class="menu-sub">
                    <li class="menu-item {{ request()->routeIs('pesan-data-unit', 'edit-data-unit') ? 'active' : '' }}">
                        <a href="{{ route('pesan-data-unit') }}" class="menu-link">
                            <div data-i18n="data_unit-aktif">Perubahan Data Unit</div>
                        </a>
                    </li>
                    <li class="menu-item {{ request()->routeIs('riwayatPerubahanDataUnit') ? 'active' : '' }}">
                        <a href="{{ route('riwayatPerubahanDataUnit') }}" class="menu-link">
                            <div data-i18n="riwayat-data_unit">Riwayat Perubahan Data Unit</div>
                        </a>
                    </li>
                </ul>
            </li>
        @endif
        <!-- Riwayat Permintaan -->
        <li class="menu-item {{ request()->routeIs('messageRequest') ? 'active' : '' }}">
            <a href="{{ route('messageRequest') }}" class="menu-link">
                <i class="menu-icon bx bx-history"></i>
                <div data-i18n="data-unit">Permintaan</div>
            </a>
        </li>
        @if ($loggedInUser->role->role == 'supervisor')
            <li
            class="menu-item {{ request()->routeIs('riwayatPerubahanPeminjaman', 'riwayatPerubahanPerbaikan', 'riwayatPerubahanDataUnit') ? 'active open' : '' }}">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class='menu-icon bx bx-message-alt-edit'></i>
                <div data-i18n="peminjaman">Riwayat</div>
            </a>
            <ul class="menu-sub">
                <li class="menu-item {{ request()->routeIs('riwayatPerubahanPeminjaman') ? 'active' : '' }}">
                    <a href="{{ route('riwayatPerubahanPeminjaman') }}" class="menu-link">
                        <div data-i18n="riwayat-peminjaman">Peminjaman</div>
                    </a>
                </li>
                <li class="menu-item {{ request()->routeIs('riwayatPerubahanPerbaikan') ? 'active' : '' }}">
                    <a href="{{ route('riwayatPerubahanPerbaikan') }}" class="menu-link">
                        <div data-i18n="riwayat-peminjaman">Perbaikan</div>
                    </a>
                </li>
                <li class="menu-item {{ request()->routeIs('riwayatPerubahanDataUnit') ? 'active' : '' }}">
                    <a href="{{ route('riwayatPerubahanDataUnit') }}" class="menu-link">
                        <div data-i18n="riwayat-peminjaman">Data Unit</div>
                    </a>
                </li>
            </ul>
        </li>
        @endif
    </ul>
</aside>
