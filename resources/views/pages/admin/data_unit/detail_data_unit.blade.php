<div class="modal fade" id="dataildataunit{{ $asset->asset_id }}" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel3">Detail Data Unit</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <hr>
            <div class="modal-body">
                <form>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">No. Unit</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $asset->no_unit }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">Tipe</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $asset->type_name }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">Merek</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $asset->manufacture }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">Model</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $asset->model  }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">Tahun</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $asset->yom }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">STNK</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $asset->stnk_issued }} - {{ $asset->stnk_expired }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">KIR</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $asset->kir_issued }} - {{ $asset->kir_expired }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">Tipe Asuransi</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $asset->insurance_type_name }}"/>
                        </div>
                    </div>
                    <div class="row mb-3 ms-2">
                        <label class="col-sm-2 col-form-label" for="basic-default-name">Asuransi</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="basic-default-name" disabled value="{{ $asset->insurance_issued }} - {{ $asset->insurance_expired }}"/>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
