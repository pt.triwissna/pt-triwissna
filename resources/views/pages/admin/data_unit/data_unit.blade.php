@extends('app')
@section('title-app')
    Data Unit
@endsection
@section('navbar-title-back')
    Data Unit
@endsection
@section('content')
    @if (session('success'))
        @include('partials.alert-success')
    @endif

    @if (session('error'))
        @include('partials.alert-error')
    @endif
    <div class="card p-3">
        <form action="{{ route('data-unit') }}" method="GET">
            @if ($loggedInUser->role->role == 'admin')
                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                    <a href="{{ route('add-data-unit') }}" class="btn btn-primary me-md-2 pe-5 ps-5">Tambah</a>
                </div>
            @endif
            <div class="row mt-4 mb-5">
                <div class="col-md-3">
                    <div>
                        <label for="filter" class="fw-bold">Nomor Unit</label>
                        <input type="text" class="form-control" id="defaultFormControlInput" placeholder="Nomor Unit"
                            aria-describedby="defaultFormControlHelp" name="no_unit"
                            value="{{ isset($_GET['no_unit']) ? $_GET['no_unit'] : '' }}" />

                    </div>
                </div>
                <div class="col-md-3">
                    <div>
                        <label for="filter" class="fw-bold">Tipe</label>
                        <select class="form-select" name="id_type" id="exampleFormControlSelect1"
                            aria-label="Default select example">
                            <option selected value="">- Semua -</option>
                            @foreach ($typeAssets as $o)
                                <option value="{{ $o->id_type }}"
                                    {{ isset($_GET['id_type']) && (int) $_GET['id_type'] === $o->id_type ? 'selected' : '' }}>
                                    {{ $o->type_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div>
                        <label for="filter" class="fw-bold">Status</label>
                        <select class="form-select" name="flg_status" id="exampleFormControlSelect1"
                            aria-label="Default select example">
                            <option selected value="">- Semua -</option>
                            <option value="T"
                                {{ isset($_GET['flg_status']) && $_GET['flg_status'] == 'T' ? 'selected' : '' }}>Tersedia
                            </option>
                            <option value="B"
                                {{ isset($_GET['flg_status']) && $_GET['flg_status'] == 'B' ? 'selected' : '' }}>Beroperasi
                            </option>
                            <option value="P"
                                {{ isset($_GET['flg_status']) && $_GET['flg_status'] == 'P' ? 'selected' : '' }}>Perawatan
                            </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-1  d-flex align-items-end">
                    <button type="submit" class="btn btn-primary">Cari</button>
                </div>
        </form>
    </div>
    <div class="table-responsive text-nowrap">
        <table class="table table-striped table-hover">
            <thead>
                <tr class="table-active">
                    <th class="fw-bold">Aksi</th>
                    <th class="fw-bold">No. Unit</th>
                    <th class="fw-bold">Tipe</th>
                    <th class="fw-bold">Kategori</th>
                    <th class="fw-bold">Merek & Model</th>
                    <th class="fw-bold">Status</th>
                </tr>
            </thead>
            <tbody class="table-border-bottom-0">
                @if (count($assets) < 1)
                    <tr>
                        <td colspan="7" style="padding: 20px; font-size: 20px;"><span>Tidak ditemukan data</span>
                        </td>
                    </tr>
                @else
                    @foreach ($assets as $asset)
                        @include('pages.admin.data_unit.detail_data_unit')
                        @include('pages.admin.data_unit.revisi_data_unit')
                        <tr>
                            <td>
                                <button class="btn btn-primary btn-sm" type="button" data-bs-toggle="modal"
                                    data-bs-target="#dataildataunit{{ $asset->asset_id }}">DETAIL</button>
                                @if ($loggedInUser->role->role == 'admin' || $loggedInUser->role->role == 'supervisor')
                                    @if ($asset->flg_action == 'H' || $asset->flg_action == null)
                                        <button class="btn btn-danger btn-sm" type="button" data-bs-toggle="modal"
                                            data-bs-target="#revisidataunit{{ $asset->asset_id }}">REVISI</button>
                                    @else
                                        <button class="btn btn-danger btn-sm" type="button" data-bs-toggle="modal"
                                            data-bs-target="#revisidataunit{{ $asset->asset_id }}" disabled>REVISI</button>
                                    @endif
                                @endif

                            </td>
                            <td>{{ $asset->no_unit }}</td>
                            <td>{{ $asset->type->type_name }}</td>
                            <td>{{ $asset->ctgr->ctgr_name }}</td>
                            <td>{{ $asset->manufacture }} - {{ $asset->model }}</td>
                            <td>
                                @php
                                    $statusText = '';
                                    $statusClass = '';
                                    
                                    switch ($asset->flg_status) {
                                        case 'T':
                                            $statusText = 'Tersedia';
                                            $statusClass = 'bg-success';
                                            break;
                                        case 'B':
                                            $statusText = 'Beroperasi';
                                            $statusClass = 'bg-warning';
                                            break;
                                        case 'P':
                                            $statusText = 'Perawatan';
                                            $statusClass = 'bg-danger';
                                            break;
                                        default:
                                            $statusText = 'Status Tidak Valid';
                                            $statusClass = 'bg-secondary';
                                            break;
                                    }
                                @endphp

                                <span class="badge rounded-pill {{ $statusClass }}">{{ $statusText }}</span>
                            </td>
                        </tr>
                    @endforeach
                @endif


            </tbody>
        </table>
    </div>
    <div class="row pt-5">
        <div class="col-lg-10">
            <ul class="pagination">
                {{ $assets->links() }}
            </ul>
            <br>
            <span>Total data {{ $totalData[0]->total_count }}, halaman {{ $assets->currentPage() }} dari
                {{ $assets->lastPage() }}</span>

        </div>
    </div>
    </div>
@endsection
