<form action="{{ route('revisi_pesan_asset', ['id' => $asset->asset_id]) }}" method="post">
    @csrf
<div class="modal fade" id="revisidataunit{{ $asset->asset_id }}" aria-hidden="true" aria-labelledby="modalToggleLabel2" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalToggleLabel2">Permintaan Revisi Data Unit kepada Supervisor</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <hr class="line">
            <div class="modal-body">
                <div class="row mb-3">
                    <div class="col-md-4">
                        <label for="hari/tanggal" class="form-label">Hari/Tanggal</label>
                    </div>
                    <div class="col-md-8">
                        <input type="date" id="hari/tanggal" class="form-control" name="date_message" value="{{ date('Y-m-d') }}">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <label for="shift" class="form-label">Judul</label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" id="hari/tanggal" class="form-control" name="title_message">
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4">
                        <label for="pesan" class="form-label">Pesan</label>
                    </div>
                    <div class="col-md-8">
                        <textarea id="pesan" class="form-control" rows="3" name="contents_message"></textarea>
                    </div>
                </div>
            </div>


            <div class="modal-footer">
                <button class="btn btn-info" data-bs-dismiss="modal">Batal</button>
                <button class="btn btn-success">Kirim</button>
            </div>
        </div>
    </div>
</div>
</form>