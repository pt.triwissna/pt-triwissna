<!DOCTYPE html>
<html lang="en" class="light-style layout-menu-fixed" dir="ltr" data-theme="theme-default"
    data-assets-path="../assets/" data-template="vertical-menu-template-free">

<head>
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title>PT Triwisnna - @yield('title-app')</title>

    <meta name="description" content="" />

    <!-- Favicon -->
    @include('link-to-asset.styleLink')
</head>

<body>
    <div class="loading-overlay">
        <div class="load-bar">
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
        </div>
    </div>

    <!-- Layout wrapper -->
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">
            <!-- Menu -->

            @include('partials.sidebar')
            <!-- / Menu -->

            <!-- Layout container -->
            <div class="layout-page">
                <!-- Navbar -->

                @include('partials.navbar')

                <!-- / Navbar -->

                <!-- Content wrapper -->
                <div class="content-wrapper">
                    <div class="container-xxl flex-grow-2 container-p-y">
                        <!-- Content -->

                        @yield('content')
                        <!-- / Content -->
                    </div>
                    <!-- Footer -->
                    @include('partials.footer')
                    <!-- / Footer -->

                    <div class="content-backdrop fade"></div>
                </div>
                <!-- Content wrapper -->
            </div>
            <!-- / Layout page -->
        </div>

        <!-- Overlay -->
        <div class="layout-overlay layout-menu-toggle"></div>
    </div>
    <!-- / Layout wrapper -->
    <div class="buy-now">
        @if ($loggedInUser->role->role == 'admin')
        <a href="{{ route('pesan-perubahan-peminjaman') }}" class="btn-buy-now">
            <svg xmlns="http://www.w3.org/2000/svg" width="73" height="73" viewBox="0 0 73 73" fill="none">
                <circle cx="36.5" cy="36.5" r="36.5" fill="#696CFF" />
                <path fill-rule="evenodd" clip-rule="evenodd"
                    d="M19.4018 39.0694H41.9697C42.7544 39.0694 43.3871 38.3745 43.3871 37.5173V20.4136C43.3871 19.5571 42.7525 18.8615 41.9697 18.8615H16.4174C15.6327 18.8615 15 19.5564 15 20.4136V37.8615V44.4259L19.4018 39.0694Z"
                    fill="white" />
                <path fill-rule="evenodd" clip-rule="evenodd"
                    d="M19.4018 38.2079H41.9697C42.7544 38.2079 43.3871 37.513 43.3871 36.6558V19.5521C43.3871 18.6956 42.7525 18 41.9697 18H16.4174C15.6327 18 15 18.6949 15 19.5521V37V43.5643L19.4018 38.2079Z"
                    fill="white" />
                <path fill-rule="evenodd" clip-rule="evenodd"
                    d="M54.5982 50.6437H32.0303C31.2456 50.6437 30.6129 49.9487 30.6129 49.0915V31.9879C30.6129 31.1313 31.2475 30.4357 32.0303 30.4357H57.5826C58.3673 30.4357 59 31.1306 59 31.9879V49.4357V56.0001L54.5982 50.6437Z"
                    fill="#898BFF" />
                <path
                    d="M39.8387 42.094C40.6226 42.094 41.258 41.398 41.258 40.5395C41.258 39.681 40.6226 38.985 39.8387 38.985C39.0548 38.985 38.4193 39.681 38.4193 40.5395C38.4193 41.398 39.0548 42.094 39.8387 42.094Z"
                    fill="#D8D8D8" />
                <path
                    d="M24.2258 29.6584C25.0096 29.6584 25.6451 28.9625 25.6451 28.104C25.6451 27.2455 25.0096 26.5495 24.2258 26.5495C23.4419 26.5495 22.8064 27.2455 22.8064 28.104C22.8064 28.9625 23.4419 29.6584 24.2258 29.6584Z"
                    fill="#D8D8D8" />
                <path
                    d="M44.8064 42.094C45.5903 42.094 46.2258 41.398 46.2258 40.5395C46.2258 39.681 45.5903 38.985 44.8064 38.985C44.0226 38.985 43.3871 39.681 43.3871 40.5395C43.3871 41.398 44.0226 42.094 44.8064 42.094Z"
                    fill="#D8D8D8" />
                <path
                    d="M29.1935 29.6584C29.9774 29.6584 30.6129 28.9625 30.6129 28.104C30.6129 27.2455 29.9774 26.5495 29.1935 26.5495C28.4096 26.5495 27.7742 27.2455 27.7742 28.104C27.7742 28.9625 28.4096 29.6584 29.1935 29.6584Z"
                    fill="#D8D8D8" />
                <path
                    d="M49.7742 42.094C50.5581 42.094 51.1936 41.398 51.1936 40.5395C51.1936 39.681 50.5581 38.985 49.7742 38.985C48.9903 38.985 48.3549 39.681 48.3549 40.5395C48.3549 41.398 48.9903 42.094 49.7742 42.094Z"
                    fill="#D8D8D8" />
                <path
                    d="M34.1613 29.6584C34.9452 29.6584 35.5807 28.9625 35.5807 28.104C35.5807 27.2455 34.9452 26.5495 34.1613 26.5495C33.3774 26.5495 32.7419 27.2455 32.7419 28.104C32.7419 28.9625 33.3774 29.6584 34.1613 29.6584Z"
                    fill="#D8D8D8" />
            </svg>
        </a>
        @endif
        @if ($loggedInUser->role->role == 'supervisor')
        <a href="{{ route('messageRequest') }}" class="btn-buy-now">
            <svg xmlns="http://www.w3.org/2000/svg" width="73" height="73" viewBox="0 0 73 73" fill="none">
                <circle cx="36.5" cy="36.5" r="36.5" fill="#696CFF" />
                <path fill-rule="evenodd" clip-rule="evenodd"
                    d="M19.4018 39.0694H41.9697C42.7544 39.0694 43.3871 38.3745 43.3871 37.5173V20.4136C43.3871 19.5571 42.7525 18.8615 41.9697 18.8615H16.4174C15.6327 18.8615 15 19.5564 15 20.4136V37.8615V44.4259L19.4018 39.0694Z"
                    fill="white" />
                <path fill-rule="evenodd" clip-rule="evenodd"
                    d="M19.4018 38.2079H41.9697C42.7544 38.2079 43.3871 37.513 43.3871 36.6558V19.5521C43.3871 18.6956 42.7525 18 41.9697 18H16.4174C15.6327 18 15 18.6949 15 19.5521V37V43.5643L19.4018 38.2079Z"
                    fill="white" />
                <path fill-rule="evenodd" clip-rule="evenodd"
                    d="M54.5982 50.6437H32.0303C31.2456 50.6437 30.6129 49.9487 30.6129 49.0915V31.9879C30.6129 31.1313 31.2475 30.4357 32.0303 30.4357H57.5826C58.3673 30.4357 59 31.1306 59 31.9879V49.4357V56.0001L54.5982 50.6437Z"
                    fill="#898BFF" />
                <path
                    d="M39.8387 42.094C40.6226 42.094 41.258 41.398 41.258 40.5395C41.258 39.681 40.6226 38.985 39.8387 38.985C39.0548 38.985 38.4193 39.681 38.4193 40.5395C38.4193 41.398 39.0548 42.094 39.8387 42.094Z"
                    fill="#D8D8D8" />
                <path
                    d="M24.2258 29.6584C25.0096 29.6584 25.6451 28.9625 25.6451 28.104C25.6451 27.2455 25.0096 26.5495 24.2258 26.5495C23.4419 26.5495 22.8064 27.2455 22.8064 28.104C22.8064 28.9625 23.4419 29.6584 24.2258 29.6584Z"
                    fill="#D8D8D8" />
                <path
                    d="M44.8064 42.094C45.5903 42.094 46.2258 41.398 46.2258 40.5395C46.2258 39.681 45.5903 38.985 44.8064 38.985C44.0226 38.985 43.3871 39.681 43.3871 40.5395C43.3871 41.398 44.0226 42.094 44.8064 42.094Z"
                    fill="#D8D8D8" />
                <path
                    d="M29.1935 29.6584C29.9774 29.6584 30.6129 28.9625 30.6129 28.104C30.6129 27.2455 29.9774 26.5495 29.1935 26.5495C28.4096 26.5495 27.7742 27.2455 27.7742 28.104C27.7742 28.9625 28.4096 29.6584 29.1935 29.6584Z"
                    fill="#D8D8D8" />
                <path
                    d="M49.7742 42.094C50.5581 42.094 51.1936 41.398 51.1936 40.5395C51.1936 39.681 50.5581 38.985 49.7742 38.985C48.9903 38.985 48.3549 39.681 48.3549 40.5395C48.3549 41.398 48.9903 42.094 49.7742 42.094Z"
                    fill="#D8D8D8" />
                <path
                    d="M34.1613 29.6584C34.9452 29.6584 35.5807 28.9625 35.5807 28.104C35.5807 27.2455 34.9452 26.5495 34.1613 26.5495C33.3774 26.5495 32.7419 27.2455 32.7419 28.104C32.7419 28.9625 33.3774 29.6584 34.1613 29.6584Z"
                    fill="#D8D8D8" />
            </svg>
        </a>
        @endif
    </div>


    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="{{ asset('vendor/libs/jquery/jquery.js') }}"></script>
    <script src="{{ asset('vendor/libs/popper/popper.js') }}"></script>
    <script src="{{ asset('vendor/js/bootstrap.js') }}"></script>
    <script src="{{ asset('vendor/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>

    <script src="{{ asset('vendor/js/menu.js') }}"></script>
    <!-- endbuild -->

    <!-- Main JS -->
    <script src="{{ asset('js/main.js') }}"></script>

    <script>
        $(window).on("load", function() {
            $(".loading-overlay").fadeOut("slow");
        });
    </script>

    <!-- Page JS -->

    <!-- Place this tag in your head or just before your close body tag. -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
</body>

</html>
