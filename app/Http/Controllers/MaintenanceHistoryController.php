<?php

namespace App\Http\Controllers;

use App\Models\CategoryAssetModels;
use App\Models\MaintenanceHistoryModel;
use App\Models\TypeAssetModels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \Barryvdh\DomPDF\Facade\PDF;

class MaintenanceHistoryController extends Controller
{
    public function index(Request $request)
    {

        $query = MaintenanceHistoryModel::query()
            ->select('A.*', 'B.*', 'C.*', 'D.*')
            ->from('maintenance_history AS A')
            ->join('assets AS B', 'A.asset_id', '=', 'B.asset_id')
            ->join('type_asset AS C', 'B.id_type', '=', 'C.id_type')
            ->join('category_asset AS D', 'B.id_ctgr', '=', 'D.id_ctgr')
            ->when($request->no_unit, function ($query, $no_unit) {
                return $query->where('B.no_unit', 'like', '%' . $no_unit . '%');
            })
            ->when($request->s_breakdown_date, function ($query, $s_breakdown_date) {
                return $query->where('A.s_breakdown_date', $s_breakdown_date);
            })
            ->when($request->id_type, function ($query, $id_type) {
                return $query->where('B.id_type', $id_type);
            })
            ->orderBy('A.s_breakdown_date', 'asc');;

        $MaintenanceHistory = $query->paginate(20);

        $queryType = "
            SELECT * 
            FROM type_asset
        ";
        $typeAssets = DB::select($queryType);

        $countQuery = "
            SELECT COUNT(1) AS total_count
            FROM maintenance_history
        ";

        $totalData = DB::select($countQuery);

        return view('pages.admin.riwayat_perbaikan.riwayat_perbaikan', compact(['MaintenanceHistory', 'typeAssets', 'totalData']));
    }
    public function generatePDF($id)
    {
        $maintenancehistory = MaintenanceHistoryModel::with(['asset'])
            ->find($id);
        $id_asset = $maintenancehistory->asset->id_type;
        $id_category = $maintenancehistory->asset->id_ctgr;
        $typeAsset = TypeAssetModels::find($id_asset);
        $typeCtgr = CategoryAssetModels::find($id_category);

        $pdf = PDF::loadView('pages.admin.riwayat_perbaikan.print', compact('typeCtgr', 'maintenancehistory', 'typeAsset'));

        return $pdf->download('DetailRiwayatPerbaikan.pdf');
    }
}
