<?php

namespace App\Http\Controllers;

use App\Models\AssetsModel;
use App\Models\CategoryAssetModels;
use App\Models\OperationHistoryModels;
use App\Models\TypeAssetModels;
use Illuminate\Http\Request;
use App\Models\OperationActive;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use \Barryvdh\DomPDF\Facade\PDF;


class OperationActiveController extends Controller
{

    public function generatePDF($id)
    {
        $operationshistory = OperationHistoryModels::with(['asset'])
            ->find($id);
        $id_asset = $operationshistory->asset->id_type;
        $id_category = $operationshistory->asset->id_ctgr;
        $typeAsset = TypeAssetModels::find($id_asset);
        $typeCtgr = CategoryAssetModels::find($id_category);

        $pdf = PDF::loadView('pages.admin.riwayat_peminjaman.print', compact('typeCtgr', 'operationshistory', 'typeAsset'));

        return $pdf->download('DetailRiwayatPeminjaman.pdf');
    }
    public function index(Request $request)
    {
        $query = "
            SELECT A.id_operation_active, A.asset_id, A.date, A.shift, A.s_hourmeter, A.s_work_time, A.f_hourmeter, 
                A.f_work_time, A.s_otherdelay, A.s_work_time, A.f_otherdelay, A.s_odometer, A.f_odometer, A.fuel_inflow, 
                A.project, A.operator, A.trip, A.record_adm_id, B.id_type, B.id_ctgr, B.id_permits_insurance, B.manufacture, 
                B.model, B.yom, B.no_unit, B.flg_status, C.type_name, D.ctgr_name, MAX(E.flg_action) AS flg_action
            FROM operation_active A
            INNER JOIN assets B ON A.asset_id = B.asset_id
            INNER JOIN type_asset C ON B.id_type = C.id_type
            INNER JOIN category_asset D ON B.id_ctgr = D.id_ctgr
            LEFT JOIN operations_messages E ON A.id_operation_active = E.id_operation_active
            WHERE true
        ";


        if ($request->no_unit != NULL) {
            $no_unit = $request->no_unit;
            $query .= " AND B.no_unit LIKE '%$no_unit%'";
        }

        if ($request->shift != NULL) {
            $shift = $request->shift;
            $query .= " AND A.shift = '$shift'";
        }

        $query .= " GROUP BY A.id_operation_active, A.asset_id, A.date, A.shift, A.s_hourmeter, A.s_work_time, A.f_hourmeter, 
                A.f_work_time, A.s_otherdelay, A.s_work_time, A.f_otherdelay, A.s_odometer, A.f_odometer, A.fuel_inflow, 
                A.project, A.operator, A.trip, A.record_adm_id, B.id_type, B.id_ctgr, B.id_permits_insurance, B.manufacture, 
                B.model, B.yom, B.no_unit, B.flg_status, C.type_name, D.ctgr_name";
        $query .= " ORDER BY A.s_work_time ASC";


        $queryCount = "
            SELECT COUNT(1) AS totalData
            FROM operation_active
        ";

        $total = DB::select($queryCount);

        $OperationActive = DB::select($query);
        return view('pages.admin.peminjaman.peminjaman_aktif', compact('OperationActive', 'total'));
    }

    public function tambah_data()
    {
        $queryAsset = '
            SELECT * 
            FROM assets 
            WHERE flg_status = \'T\'
            ORDER BY manufacture ASC
        ';

        $getAsset = DB::select($queryAsset);
        return view('pages.admin.peminjaman.peminjaman_tambah', compact('getAsset'));
    }

    public function insert_data(Request $request)
    {
        $asset_id = $request->asset_id;
        // Lakukan validasi data
        $validatedData = $request->validate([
            'date' => 'required|date',
            'shift' => 'required|in:DAY,NIGHT',
            'asset_id' => 'required|integer',
            's_hourmeter' => 'nullable|integer',
            's_work_time' => 'required',
            's_otherdelay' => 'nullable',
            's_odometer' => 'nullable|integer',
            'fuel_inflow' => 'nullable|integer',
            'project' => 'required|string',
            'operator' => 'required|string',
            'trip' => 'nullable|integer',
        ]);

        $user_id = Auth::user()->id_user;

        // Lanjutkan dengan proses penyimpanan data ke database
        // Buat array untuk di-insert ke dalam tabel
        $data = [
            'date' => $validatedData['date'],
            'shift' => $validatedData['shift'],
            'asset_id' => $validatedData['asset_id'],
            's_hourmeter' => $validatedData['s_hourmeter'],
            's_work_time' => $validatedData['s_work_time'],
            's_odometer' => $validatedData['s_odometer'],
            'project' => $validatedData['project'],
            'operator' => $validatedData['operator'],
            'record_adm_id' => $user_id,
        ];

        // Simpan data ke dalam tabel PeminjamanAktif menggunakan model
        OperationActive::create($data);

        // ubah flg asset
        $flgAsset = AssetsModel::find($asset_id);
        if ($flgAsset) {
            $flgAsset->update([
                'flg_status' => 'B'
            ]);
        }

        // Setelah penyimpanan berhasil, redirect atau berikan pesan sesuai kebutuhan
        return redirect()->route('peminjaman-aktif')->with('success', 'Data berhasil disimpan.');
        // Jika penyimpanan gagal, redirect atau berikan pesan sesuai kebutuhan
        // return redirect()->back()->with('error', 'Terjadi kesalahan. Data gagal disimpan.');
    }


    public function insert_peminjaman_selesai(Request $request, $id_operation_active)
    {
        try {
            // Lakukan validasi data
            $validatedData = $request->validate([
                'f_work_time' => 'required',
                'f_odometer' => 'nullable|integer',
                'f_hourmeter' => 'nullable|integer',
                'fuel_inflow' => 'required|integer',
                'trip' => 'required|integer',
                's_otherdelay' => 'nullable',
                'f_otherdelay' => 'nullable',
            ]);

            // Jika validasi berhasil, data akan mencapai baris ini

            // Dapatkan instance objek model berdasarkan ID operasi aktif
            $operationActive = OperationActive::find($id_operation_active);

            $asset_id = $operationActive->asset_id;

            // Pastikan instance ditemukan sebelum memanggil metode update
            if ($operationActive) {
                $operationActive->update([
                    'f_work_time' => $validatedData['f_work_time'],
                    'f_odometer' => $validatedData['f_odometer'],
                    'f_hourmeter' => $validatedData['f_hourmeter'],
                    'fuel_inflow' => $validatedData['fuel_inflow'],
                    'trip' => $validatedData['trip'],
                    's_otherdelay' => $validatedData['s_otherdelay'],
                    'f_otherdelay' => $validatedData['f_otherdelay'],
                ]);

                $ambil_data = OperationActive::all()->map(function ($item) {
                    unset($item['id_operation_active']);
                    return $item;
                });

                $flgAsset = AssetsModel::find($asset_id);
                if ($flgAsset) {
                    $flgAsset->update([
                        'flg_status' => 'T'
                    ]);
                }

                foreach ($ambil_data as $data) {
                    OperationHistoryModels::create($data->toArray());
                }

                $operationActive->delete();
                return redirect()->route('peminjaman-aktif')->with('success', 'Data berhasil diupdate atau disimpan.');
            } else {
                return redirect()->route('peminjaman-aktif')->with('error', 'Operasi aktif tidak ditemukan.');
            }
        } catch (ValidationException $e) {

            return redirect()->route('peminjaman-aktif');
        }
    }

    public function edit_data(Request $request, $id, $asset_id)
    {
        $getAsset = AssetsModel::find($asset_id);
        $operationActive = OperationActive::find($id);
        return view('pages.admin.peminjaman.peminjaman_ubah', compact('operationActive', 'getAsset'));
    }


    public function update_data(Request $request, $id)
    {
        $validatedData = $request->validate([
            'date' => 'required|date',
            'shift' => 'required|in:DAY,NIGHT',
            'asset_id' => 'required|integer',
            's_hourmeter' => 'nullable|integer',
            's_work_time' => 'required',
            's_otherdelay' => 'nullable',
            's_odometer' => 'nullable|integer',
            'fuel_inflow' => 'nullable|integer',
            'project' => 'required|string',
            'operator' => 'required|string',
            'trip' => 'nullable|integer',
        ]);

        // Dapatkan instance objek model berdasarkan ID operasi aktif
        $operationActive = OperationActive::find($id);

        // Pastikan instance ditemukan sebelum memanggil metode update
        if ($operationActive) {
            $operationActive->update([
                'date' => $validatedData['date'],
                'shift' => $validatedData['shift'],
                'asset_id' => $validatedData['asset_id'],
                's_hourmeter' => isset($validatedData['s_hourmeter']),
                's_work_time' => $validatedData['s_work_time'],
                's_otherdelay' => isset($validatedData['s_otherdelay']),
                's_odometer' => isset($validatedData['s_odometer']),
                'fuel_inflow' => isset($validatedData['fuel_inflow']),
                'project' => $validatedData['project'],
                'operator' => $validatedData['operator'],
                'trip' => isset($validatedData['trip']),
            ]);

            // Redirect ke halaman peminjaman aktif dengan pesan sukses
            return redirect()->route('peminjaman-aktif')->with('success', 'Data berhasil diupdate.');
        } else {
            // Redirect ke halaman peminjaman aktif dengan pesan error
            return redirect()->route('peminjaman-aktif')->with('error', 'Operasi aktif tidak ditemukan.');
        }
    }
}