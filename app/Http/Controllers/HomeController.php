<?php

namespace App\Http\Controllers;

use App\Models\MaintenanceActiveModel;
use App\Models\OperationActive;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
  public function index()
  {
    // total operation
    $queryOperation = "
          SELECT COUNT(1) AS total_operation
          FROM operation_active
        ";
    $operation = DB::select($queryOperation);
    // total maintenance
    $queryMain = "
          SELECT COUNT(1) AS total_maintenace
          FROM maintenance_active
        ";
    $maintenace = DB::select($queryMain);


    // get Operation for supervisor
    $queryGetOperationSpv = "
          SELECT *
          FROM operations_messages
          WHERE for_to = 'supervisor'
        ";
    $getOperationsSpv = DB::select($queryGetOperationSpv);
    // get Maintenance for supervisor
    $queryGetMaintenanceSpv = "
          SELECT *
          FROM maintenance_messages
          WHERE for_to = 'supervisor'
        ";
    $getMaintenanceSpv = DB::select($queryGetMaintenanceSpv);
    // get Data Unit for supervisor
    $querydataUnitSpv = "
          SELECT *
          FROM data_unit_messages
          WHERE for_to = 'supervisor'
        ";
    $getDataUnitSpv = DB::select($querydataUnitSpv);


    // get Operation for admin
    $queryGetOPerationAdm = "
          SELECT *
          FROM operations_messages
          WHERE for_to = 'admin'
          AND flg_action = 'Y'
        ";
    $getOperationsAdm = DB::select($queryGetOPerationAdm);
    // get Maintenance for admin
    $queryMaintenanceAdm = "
          SELECT *
          FROM maintenance_messages
          WHERE for_to = 'admin'
          AND flg_action = 'Y'
        ";
    $getMaintenanceAdm = DB::select($queryMaintenanceAdm);

    // get DataUnit for admin
    $queryDataUnitAdm = "
          SELECT *
          FROM data_unit_messages
          WHERE for_to = 'admin'
          AND flg_action = 'Y'
        ";
    $getDataUnitAdm = DB::select($queryDataUnitAdm);

    return view('pages.admin.home', compact([
      'operation', 'maintenace', 'getOperationsSpv', 'getMaintenanceSpv',
      'getOperationsAdm', 'getMaintenanceAdm', 'getDataUnitAdm', 'getDataUnitSpv'
    ]));
  }

  public function getRealtimeData()
  {
    $totalPeminjaman = DB::table('operation_active')->count();
    $totalPerbaikan = DB::table('maintenance_active')->count();

    return response()->json([
      'total_peminjaman' => $totalPeminjaman,
      'total_perbaikan' => $totalPerbaikan,
    ]);
  }
}
