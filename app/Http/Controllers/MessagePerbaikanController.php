<?php

namespace App\Http\Controllers;

use App\Models\MaintenanceMessage;
use App\Models\MaintenanceActiveModel;
use App\Models\AssetsModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MessagePerbaikanController extends Controller
{
    public function pesanMaintenance()
    {
        $maintenanceMessages = MaintenanceMessage::where('flg_action', 'Y')->get();
        return view('pages.admin.message_adm.pages.admin.perbaikan.perubahan_perbaikan', compact('maintenanceMessages'));
    }

    public function riwayatPerbaikan(Request $request)
    {
        $date_message = $request->date_message;
        $query = "
            SELECT *
            FROM maintenance_messages
            WHERE flg_action = 'H'
        ";
        if($date_message != NULL) {
            $query .= " AND date_message = '$date_message'";
        }
        
        $riwayatPerbaikan = DB::select($query);
        return view('pages.admin.message_adm.pages.admin.perbaikan.riwayat_perubahan_perbaikan', compact('riwayatPerbaikan'));
    }

    public function riwayatPerbaikanJson()
    {
        $riwayatPerbaikan = MaintenanceMessage::where('flg_action', 'H')->get();
        return response()->json($riwayatPerbaikan);
    }

    public function getMaintenanceMessages()
    {
    $maintenanceMessages = MaintenanceMessage::where('flg_action', 'Y')->get(); // Ambil semua data dari tabel operations_messages
        return response()->json($maintenanceMessages);
    }

    public function editmaintenance($id)
    {
        $message = MaintenanceMessage::find($id);
        $id_active = $message->id_maintenance_active;
        $maintenanceActive = MaintenanceActiveModel::find($id_active);
        $id_asset = $maintenanceActive->asset_id;
        $asset = AssetsModel::find($id_asset);
        $id_maintenance = $id;
        return view('pages.admin.message_adm.pages.admin.perbaikan.perubahan_perbaikan_edit', compact('maintenanceActive', 'asset', 'id_maintenance'));
    }

    public function update_pesanPerbaikan(Request $request, $id, $id_message)
    {
        $validatedData = $request->validate([
            's_breakdown_date'  => 'required|date',
            's_breakdown_time'  => 'required',
            'asset_id'          => 'required|integer',
            'issue'             => 'required',
            'perform_by'        => 'required',
            'pesan' => 'nullable|string',
        ]);
        $user_id = Auth::user()->id_user;
        $maintenancenActive = MaintenanceActiveModel::find($id);
        if ($maintenancenActive) {
            $maintenancenActive->update([
            's_breakdown_date'  => $validatedData['s_breakdown_date'],
            's_breakdown_time'  => $validatedData['s_breakdown_time'],
            'asset_id'          => $validatedData['asset_id'],
            'issue'             => $validatedData['issue'],
            'perform_by'        => $validatedData['perform_by'],
            'record_adm_id'     => $user_id
            ]);
            MaintenanceMessage::where('id_maintenance_message', $id_message)->update([
                'flg_action' => 'H',
                'pesan' => $validatedData['pesan'],
            ]);
            return redirect()->route('pesan-perubahan-perbaikan')->with('success', 'Data berhasil diupdate.');
        } else {
            return redirect()->route('pesan-perubahan-perbaikan')->with('error', 'Operasi aktif tidak ditemukan.');
        }
    }

    public function store_pesan_maintenace(Request $request, $id)
    {
        $userRole = view()->shared('loggedInUser')->role->role;
        if ($userRole === 'admin') {
            $for_to = "supervisor";
            $flg_action = 'N';
        } elseif ($userRole === 'supervisor') {
            $for_to = "admin";
            $flg_action = 'Y';
        } else {
            $for_to = "";
        }

        if ($for_to == "") {
            return redirect()->back()
                ->withInput()
                ->with('error', 'Terjadi kesalahan. Silakan periksa kembali data yang diisi.');
        } else {
            $validatedData = $request->validate([
                'date_message' => 'required|date',
                'title_message' => 'required|string',
                'contents_message' => 'required|string',
            ]);

            $data = [
                'date_message' => $validatedData['date_message'],
                'title_message' => $validatedData['title_message'],
                'contents_message' => $validatedData['contents_message'],
                'id_maintenance_active' => $id,
                'flg_action' => $flg_action,
                'for_to' => $for_to,
            ];

            MaintenanceMessage::create($data);

            return redirect()->route('perbaikan-aktif')->with('success', 'Pesan Terkirim.');
        }
    }
}
