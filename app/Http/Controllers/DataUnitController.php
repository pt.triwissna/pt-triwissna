<?php

namespace App\Http\Controllers;

use App\Models\AssetsModel;
use App\Models\CategoryAssetModels;
use App\Models\PermitsInsuranceModels;
use App\Models\TypeAssetModels;
use App\Models\TypeInsuranceModels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DataUnitController extends Controller
{
    public function index(Request $request)
    {
        // $assets = AssetsModel::with('permits', 'type', 'ctgr')->paginate('20');
        $query = AssetsModel::query()
            ->selectRaw('
                A.asset_id, A.id_type, A.id_ctgr, A.id_permits_insurance,
                A.manufacture, A.model, A.yom, A.no_unit, A.flg_status, B.type_name, C.ctgr_name,
                D.stnk_issued, D.stnk_expired, D.kir_issued, D.kir_expired, D.id_insurance_type, D.insurance_issued,
                D.insurance_expired, D.record_adm_id, E.insurance_type_name, MAX(F.flg_action) AS flg_action
            ')
            ->from('assets AS A')
            ->join('type_asset AS B', 'A.id_type', '=', 'B.id_type')
            ->join('category_asset AS C', 'A.id_ctgr', '=', 'C.id_ctgr')
            ->join('permits_insurance AS D', 'A.id_permits_insurance', '=', 'D.id_permits_insurance')
            ->leftjoin('type_insurance AS E', 'D.id_insurance_type', '=', 'E.id_insurance_type')
            ->leftjoin('data_unit_messages AS F', 'F.asset_id', '=', 'A.asset_id')
            ->when($request->no_unit, function ($query, $no_unit) {
                return $query->where('A.no_unit', 'like', '%'.$no_unit.'%');
            })
            ->when($request->id_type, function ($query, $id_type) {
                return $query->where('A.id_type', $id_type);
            })
            ->when($request->flg_status, function ($query, $flg_status) {
                return $query->where('A.flg_status', $flg_status);
            })
            ->groupBy('A.asset_id', 'A.id_type', 'A.id_ctgr', 'A.id_permits_insurance',
                'A.manufacture', 'A.model', 'A.yom', 'A.no_unit', 'A.flg_status', 'B.type_name', 'C.ctgr_name',
                'D.stnk_issued', 'D.stnk_expired', 'D.kir_issued', 'D.kir_expired', 'D.id_insurance_type', 'D.insurance_issued',
                'D.insurance_expired', 'D.record_adm_id', 'E.insurance_type_name')
            ->orderBy('A.manufacture', 'asc');

        $assets = $query->paginate(20);

        $queryType = '
            SELECT *
            FROM type_asset
        ';
        $typeAssets = DB::select($queryType);

        $countQuery = '
            SELECT COUNT(1) AS total_count
            FROM assets
        ';

        $totalData = DB::select($countQuery);

        return view('pages.admin.data_unit.data_unit', compact([
            'assets', 'typeAssets', 'totalData',
        ]));
    }

    public function create()
    {
        $Type = TypeAssetModels::all();
        $Ctgr = CategoryAssetModels::all();
        $InsuranceType = TypeInsuranceModels::all();

        return view('pages.admin.data_unit.add_data_unit', compact([
            'Type',
            'Ctgr',
            'InsuranceType',
        ]));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'id_type' => 'required',
            'id_ctgr' => 'required',
            'manufacture' => 'required',
            'model' => 'required',
            'yom' => 'required|integer',
            'no_unit' => 'required|unique:assets',
            'id_insurance_type' => 'nullable',
            'stnk_issued' => 'nullable|integer',
            'stnk_expired' => 'nullable|integer',
            'kir_issued' => 'nullable|integer',
            'kir_expired' => 'nullable|integer',
            'insurance_issued' => 'nullable|integer',
            'insurance_expired' => 'nullable|integer',
        ]);

        $user = Auth::user();

        // Dapatkan nilai-nilai tanggal yang boleh null
        $stnkIssued = $request->stnk_issued;
        $stnkExpired = $request->stnk_expired;
        $kirIssued = $request->kir_issued;
        $kirExpired = $request->kir_expired;
        $insuranceIssued = $request->insurance_issued;
        $insuranceExpired = $request->insurance_expired;

        // Inisialisasi new_permits ke null
        $new_permits = null;

        // Periksa apakah salah satu bidang yang boleh null memiliki nilai
        if (!is_null($stnkIssued) || !is_null($stnkExpired) || !is_null($kirIssued) || !is_null($kirExpired) || !is_null($insuranceIssued) || !is_null($insuranceExpired)) {
            // Periksa apakah tanggal kadaluarsa STNK dan KIR lebih besar dari tanggal terbit
            if ((is_null($stnkIssued) || is_null($stnkExpired) || $stnkExpired > $stnkIssued) &&
                (is_null($kirIssued) || is_null($kirExpired) || $kirExpired > $kirIssued) &&
                (is_null($insuranceIssued) || is_null($insuranceExpired) || $insuranceExpired > $insuranceIssued)
            ) {
                // Buat entri baru dalam database jika valid
                $new_permits = PermitsInsuranceModels::create([
                    'stnk_issued' => $stnkIssued,
                    'stnk_expired' => $stnkExpired,
                    'kir_issued' => $kirIssued,
                    'kir_expired' => $kirExpired,
                    'id_insurance_type' => $validatedData['id_insurance_type'],
                    'insurance_issued' => $insuranceIssued,
                    'insurance_expired' => $insuranceExpired,
                    'record_adm_id' => $user->id_user,
                ]);
            }
        }

        if ($new_permits || is_null($stnkIssued) || is_null($stnkExpired) || is_null($kirIssued) || is_null($kirExpired) || is_null($insuranceIssued) || is_null($insuranceExpired)) {
            $new_permits = PermitsInsuranceModels::create([
                'stnk_issued' => $stnkIssued,
                'stnk_expired' => $stnkExpired,
                'kir_issued' => $kirIssued,
                'kir_expired' => $kirExpired,
                'id_insurance_type' => $validatedData['id_insurance_type'],
                'insurance_issued' => $insuranceIssued,
                'insurance_expired' => $insuranceExpired,
                'record_adm_id' => $user->id_user,
            ]);

            AssetsModel::create([
                'id_type' => $request->id_type,
                'id_ctgr' => $request->id_ctgr,
                'id_permits_insurance' => $new_permits ? $new_permits->id_permits_insurance : null,
                'manufacture' => $validatedData['manufacture'],
                'model' => $validatedData['model'],
                'yom' => $validatedData['yom'],
                'no_unit' => $validatedData['no_unit'],
                'flg_status' => 'T',
            ]);

            return redirect()->route('data-unit')->with('success', 'Data berhasil disimpan.');
        } else {
            return redirect()->route('add-data-unit')->with('error', 'Tahun Terbit Harus Lebih Dulu Dari Tahun Kadaluarsa');
        }
    }
}
