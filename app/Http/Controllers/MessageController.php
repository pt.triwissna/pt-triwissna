<?php

namespace App\Http\Controllers;

use App\Models\AssetsModel;
use App\Models\CategoryAssetModels;
use App\Models\dataUnitMessageModels;
use App\Models\DataUnitModels;
use App\Models\MaintenanceActiveModel;
use App\Models\MaintenanceMessage;
use App\Models\OperationActive;
use App\Models\OperationsMessageModels;
use App\Models\PermitsInsuranceModels;
use App\Models\TypeAssetModels;
use App\Models\TypeInsuranceModels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{

    public function pesanOperations()
    {
        $operationsMessages = OperationsMessageModels::where('flg_action', 'Y')->get();
        return view('pages.admin.message_adm.pages.admin.peminjaman.perubahan_peminjaman', compact('operationsMessages'));
    }
    public function getOperationsMessages()
    {
        $operationsMessages = OperationsMessageModels::where('flg_action', 'Y')->get();
        return response()->json($operationsMessages);
    }
    public function getDataUnitMessages()
    {
        $dataUnitMessages = dataUnitMessageModels::where('flg_action', 'Y')->get();
        return response()->json($dataUnitMessages);
    }

    public function edit($id)
    {
        $message = OperationsMessageModels::find($id);
        $id_active = $message->id_operation_active;
        $operationActive = OperationActive::find($id_active);
        $id_asset = $operationActive->asset_id;
        $asset = AssetsModel::find($id_asset);
        $id_operation = $id;
        return view('pages.admin.message_adm.pages.admin.peminjaman.perubahan_peminjaman_edit', compact('operationActive', 'asset', 'id_operation'));
    }
    
    

    public function store_pesan_operations(Request $request, $id)
    {
        $userRole = view()->shared('loggedInUser')->role->role;
        if ($userRole === 'admin') {
            $for_to = "supervisor";
            $flg_action = 'N';
        } elseif ($userRole === 'supervisor') {
            $for_to = "admin";
            $flg_action = 'Y';
        } else {
            $for_to = "";
        }
        if ($for_to == "") {
            return redirect()->back()
                ->withInput()
                ->with('error', 'Terjadi kesalahan. Silakan periksa kembali data yang diisi.');
        } else {
            $validatedData = $request->validate([
                'date_message' => 'required|date',
                'title_message' => 'required|string',
                'contents_message' => 'required|string',
            ]);
            $data = [
                'date_message' => $validatedData['date_message'],
                'title_message' => $validatedData['title_message'],
                'contents_message' => $validatedData['contents_message'],
                'id_operation_active' => $id,
                'flg_action' => $flg_action,
                'for_to' => $for_to,
            ];
            OperationsMessageModels::create($data);
            return redirect()->route('peminjaman-aktif')->with('success', 'Pesan Terkirim.');
        }
    }

    public function update_pesanPeminjaman(Request $request, $id, $id_message)
    {
        $validatedData = $request->validate([
            'date' => 'required|date',
            'shift' => 'required|in:DAY,NIGHT',
            'asset_id' => 'required|integer',
            's_hourmeter' => 'nullable|integer',
            's_work_time' => 'required',
            's_otherdelay' => 'nullable',
            's_odometer' => 'nullable|integer',
            'fuel_inflow' => 'nullable|integer',
            'project' => 'required|string',
            'operator' => 'required|string',
            'pesan' => 'nullable|string',
            'trip' => 'nullable|integer',
        ]);
        $operationActive = OperationActive::find($id);
        if ($operationActive) {
            $operationActive->update([
                'date' => $validatedData['date'],
                'shift' => $validatedData['shift'],
                'asset_id' => $validatedData['asset_id'],
                's_hourmeter' => isset($validatedData['s_hourmeter']) ? $validatedData['s_hourmeter'] : null,
                's_work_time' => $validatedData['s_work_time'],
                's_otherdelay' => isset($validatedData['s_otherdelay']) ? $validatedData['s_otherdelay'] : null,
                's_odometer' => isset($validatedData['s_odometer']) ? $validatedData['s_odometer'] : null,
                'fuel_inflow' => isset($validatedData['fuel_inflow']) ? $validatedData['fuel_inflow'] : null,
                'project' => $validatedData['project'],
                'operator' => $validatedData['operator'],
                'trip' => isset($validatedData['trip']) ? $validatedData['trip'] : null,
            ]);
            OperationsMessageModels::where('id_operations_message', $id_message)->update([
                'flg_action' => 'H',
                'pesan' => $validatedData['pesan'],
            ]);
            return redirect()->route('pesan-perubahan-peminjaman')->with('success', 'Data berhasil diupdate.');
        } else {
            return redirect()->route('pesan-perubahan-peminjaman')->with('error', 'Operasi aktif tidak ditemukan.');
        }
    }

    public function get_message_request()
    {
        return view('pages.admin.message_adm.pages.admin.permintaan');
    }

    public function riwayatPeminjaman(Request $request)
    {
        // $riwayatPeminjaman = OperationsMessageModels::where('flg_action', 'H')->get();
        $date_message = $request->date_message;
        $query = "
            SELECT *
            FROM operations_messages
            WHERE flg_action = 'H'
        ";
        if($date_message != NULL) {
            $query .= " AND date_message = '$date_message'";
        }
        $riwayatPeminjaman = DB::select($query);

        return view('pages.admin.message_adm.pages.admin.peminjaman.riwayat_perubahan_peminjaman', compact('riwayatPeminjaman'));
    }

    public function riwayatPeminjamanJson()
    {
        $riwayatPeminjaman = OperationsMessageModels::where('flg_action', 'H')->get();
        return response()->json($riwayatPeminjaman);
    }


    public function riwayatDataUnit(Request $request)
    {
        $date_message = $request->date_message;
        $query = "
            SELECT * 
            FROM data_unit_messages 
            WHERE flg_action = 'H'
        ";
        if($date_message != NULL) {
            $query .= " AND date_message = '$date_message'";
        }
        $riwayatDataUnit = DB::select($query);

        return view('pages.admin.message_adm.pages.admin.dataunit.riwayat_perubahan_dataUnit', compact('riwayatDataUnit'));
    }

    public function riwayatDataUnitJson()
    {
        $riwayatDataUnit = dataUnitMessageModels::where('flg_action', 'H')->get();
        return response()->json($riwayatDataUnit);
    }
    public function pesanDataUnit()
    {
        $dataUnitMessage = dataUnitMessageModels::where('flg_action', 'Y')->get();
        return view('pages.admin.message_adm.pages.admin.dataunit.perubahan_dataUnit', compact('dataUnitMessage'));
    }

    public function revisiAsset(Request $request, $id)
    {
        $userRole = view()->shared('loggedInUser')->role->role;
        if ($userRole === 'admin') {
            $for_to = "supervisor";
        } elseif ($userRole === 'supervisor') {
            $for_to = "admin";
        } else {
            $for_to = "";
        }
        if ($for_to == "") {
            return redirect()->back()
            ->withInput()
            ->with('error', 'Terjadi kesalahan. Silakan periksa kembali data yang diisi.');
        } else {
            $validatedData = $request->validate([
                'date_message' => 'required|date',
                'title_message' => 'required|string',
                'contents_message' => 'required|string',
            ]);
            $data = [
                'date_message' => $validatedData['date_message'],
                'title_message' => $validatedData['title_message'],
                'contents_message' => $validatedData['contents_message'],
                'asset_id' => $id,
                'for_to' => $for_to,
            ];
            dataUnitMessageModels::create($data);
            return redirect()->route('data-unit')->with('success', 'Pesan Terkirim.');
        }
    }

    public function editDataUnit($id)
    {
        $message = dataUnitMessageModels::find($id);
        $id_active = $message->asset_id;
        $dataUnit = AssetsModel::find($id_active);
        $id_permits = $dataUnit->id_permits_insurance;
        $dataPermitsInsurance = PermitsInsuranceModels::find($id_permits);
        $typeAsset = TypeAssetModels::all();
        $category = CategoryAssetModels::all();
        $id_insurance = $dataPermitsInsurance->id_insurance_type;
        $typeInsurance = TypeInsuranceModels::all();
        $id_asset = $id;
        return view('pages.admin.message_adm.pages.admin.dataunit.data_unitEdit', compact(
            'dataUnit', 
            'id_asset',
            'typeAsset',
            'category',
            'typeInsurance',
            'dataPermitsInsurance'
        ));
    }




    public function getMessageRequestJson()
    {
        $userRole = view()->shared('loggedInUser')->role->role;

        // if ($userRole === 'admin') {
            $permintaan1 = OperationsMessageModels::where('flg_action', 'N')->get();
            $permintaan2 = MaintenanceMessage::where('flg_action', 'N')->get();
            $permintaan3 = dataUnitMessageModels::where('flg_action', 'N')->get();
        // } else {
        //     $permintaan1 = OperationsMessageModels::where('flg_action', 'N')
        //         ->where('for_to', '!=', 'supervisor')
        //         ->get();
        //     $permintaan2 = MaintenanceMessage::where('flg_action', 'N')
        //         ->where('for_to', '!=', 'supervisor')
        //         ->get();
        //     $permintaan3 = dataUnitMessageModels::where('flg_action', 'N')
        //         ->where('for_to', '!=', 'supervisor')
        //         ->get();
        // }
    
        $mergedData = [
            'operations' => $permintaan1,
            'maintenance' => $permintaan2,
            'asset' => $permintaan3,
        ];
    
        return response()->json($mergedData);
    }

    public function update_pesanDataUnit(Request $request, $id, $id_message)
    {
        $validatedData = $request->validate([
            'id_type'               => 'required',
            'id_ctgr'               => 'required',
            'manufacture'           => 'required',
            'model'                 => 'required',
            'yom'                   => 'required|integer',
            'no_unit'               => 'required',
            'id_insurance_type'     => 'nullable',
            'pesan'                 => 'nullable',
        ]);

        $user = Auth::user();
        $asset_dataUnit = AssetsModel::find($id);
        $id_permitsData = $asset_dataUnit->id_permits_insurance;
        // $permits_dataUnit = PermitsInsuranceModels::find($id_permitsData);
        // $new_permits = PermitsInsuranceModels::find($id_permitsData);
        PermitsInsuranceModels::where('id_permits_insurance', $id_permitsData)->update([
            'stnk_issued' => $request->stnk_issued,
            'stnk_expired' => $request->stnk_expired,
            'kir_issued' => $request->kir_issued,
            'kir_expired' => $request->kir_expired,
            'id_insurance_type' =>  $validatedData['id_insurance_type'],
            'insurance_issued' => $request->insurance_issued,
            'insurance_expired' => $request->insurance_expired,
            'record_adm_id' => $user->id_user,
        ]);
        
        AssetsModel::where('asset_id', $id)->update([
            'id_type' => $request->id_type,
            'id_ctgr' => $request->id_ctgr,
            'manufacture' => $validatedData['manufacture'],
            'model' => $validatedData['model'],
            'yom' => $validatedData['yom'],
            'no_unit' => $validatedData['no_unit'],
            'flg_status' => 'T',
        ]);

        dataUnitMessageModels::where('id_dataUnit_message', $id_message)->update([
            'flg_action' => 'H',
            'pesan' => $validatedData['pesan'],
        ]);
        return redirect()->route('pesan-data-unit')->with('success', 'Data berhasil disimpan.');
    
    }

    public function setujuOperationMessage($id)
    {
        OperationsMessageModels::find($id)->update([
            'flg_action' => 'Y',
            'for_to' => 'admin',
        ]);
        return redirect()->route('messageRequest')->with('success', 'Disetujui');
    }
    public function setujuMaintenanceMessage($id)
    {
        MaintenanceMessage::find($id)->update([
            'flg_action' => 'Y',
            'for_to' => 'admin',
        ]);
        return redirect()->route('messageRequest')->with('success', 'Disetujui');
    }
    public function setujuDataUnitMessage($id)
    {
        dataUnitMessageModels::find($id)->update([
            'flg_action' => 'Y',
            'for_to' => 'admin',
        ]);
        return redirect()->route('messageRequest')->with('success', 'Disetujui');
    }

}