<?php

namespace App\Http\Controllers;

use App\Models\OperationHistoryModels;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class OperationsHistoryControllers extends Controller
{
    public function index(Request $request)
    {
        // query filter
        $query = OperationHistoryModels::query()
            ->select('A.*', 'B.*', 'C.*', 'D.*')
            ->from('operation_history AS A')
            ->join('assets AS B', 'A.asset_id', '=', 'B.asset_id')
            ->join('type_asset AS C', 'B.id_type', '=', 'C.id_type')
            ->join('category_asset AS D', 'B.id_ctgr', '=', 'D.id_ctgr')
            ->when($request->no_unit, function ($query, $no_unit) {
                return $query->where('B.no_unit', 'like', '%' . $no_unit . '%');
            })
            ->when($request->date, function ($query, $date) {
                return $query->where('A.date', $date);
            })
            ->when($request->shift, function ($query, $shift) {
                return $query->where('A.shift', $shift);
            })
            ->orderBy('A.date', 'asc');

        $OperationsHistory = $query->paginate(20); // Jumlah item per halaman
        
        $countQuery= "
            SELECT COUNT(1) AS total_count
            FROM operation_history
        ";

        $totalData = DB::select($countQuery);

        return view('pages.admin.riwayat_peminjaman.riwayat_peminjaman', compact([
            'OperationsHistory', 'totalData'
        ]));
    }
}
