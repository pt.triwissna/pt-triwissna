<?php

namespace App\Http\Controllers;

use App\Models\AssetsModel;
use App\Models\MaintenanceActiveModel;
use App\Models\MaintenanceHistoryModel;
use App\Models\MaintenanceMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class MaintenanceActiveController extends Controller
{
    public function index(Request $request)
    {
        $no_unit = $request->no_unit;
        $date = $request->date;
        $id_type = $request->id_type;

        $query = "
            SELECT A.maintenance_active_id, A.asset_id, A.s_breakdown_date, A.s_breakdown_time, A.f_breakdown_date, A.f_breakdown_time, 
                A.issue, A.perform_by, A.perform_by, B.id_type, B.id_ctgr, B.id_permits_insurance, B.manufacture, 
                B.model, B.yom, B.no_unit, B.flg_status, C.type_name, D.ctgr_name, MAX(E.flg_action) AS flg_action
            FROM maintenance_active A
            INNER JOIN assets B ON A.asset_id = B.asset_id
            INNER JOIN type_asset C ON B.id_type = C.id_type
            INNER JOIN category_asset D ON B.id_ctgr = D.id_ctgr
            LEFT JOIN maintenance_messages E ON E.id_maintenance_active = A.maintenance_active_id
            WHERE true
        ";
        if ($no_unit != NULL) {
            $query .= " AND B.no_unit LIKE '%$no_unit%'";
        }
        if ($date != NULL) {
            $query .= " AND A.s_breakdown_date = '$date'";
        }
        if ($id_type != NULL) {
            $query .= " AND B.id_type = $id_type";
        }
        $query .= " GROUP BY A.maintenance_active_id, A.asset_id, A.s_breakdown_date, A.s_breakdown_time, A.f_breakdown_date, A.f_breakdown_time, 
            A.issue, A.perform_by, A.perform_by, B.id_type, B.id_ctgr, B.id_permits_insurance, B.manufacture, 
            B.model, B.yom, B.no_unit, B.flg_status, C.type_name, D.ctgr_name";
        $maintenanceActive = DB::select($query);

        $queryType = "
            SELECT * 
            FROM type_asset
        ";
        $typeAssets = DB::select($queryType);

        $queryCount = "
            SELECT COUNT(1) AS totalData
            FROM maintenance_active
        ";

        $total = DB::select($queryCount);

        return view('pages.admin.perbaikan_aktif.perbaikan_aktif', compact(['maintenanceActive', 'typeAssets', 'total']));
    }


    public function add_data()
    {
        $queryAsset = '
            SELECT * 
            FROM assets 
            WHERE flg_status = \'T\'
            ORDER BY manufacture ASC
        ';

        $getAsset = DB::select($queryAsset);
        return view('pages.admin.perbaikan_aktif.add_perbaikan_aktif', compact('getAsset'));
    }

    public function insert_data(Request $request)
    {
        $validatedData = $request->validate([
            's_breakdown_date'  => 'required|date',
            's_breakdown_time'  => 'required',
            'asset_id'          => 'required|integer',
            'issue'             => 'required',
            'perform_by'        => 'required',
        ]);

        $user_id = Auth::user()->id_user;

        $data = [
            's_breakdown_date'  => $validatedData['s_breakdown_date'],
            's_breakdown_time'  => $validatedData['s_breakdown_time'],
            'asset_id'          => $validatedData['asset_id'],
            'issue'             => $validatedData['issue'],
            'perform_by'        => $validatedData['perform_by'],
            'record_adm_id'     => $user_id
        ];


        MaintenanceActiveModel::create($data);

        $asset_id = $request->asset_id;
        // ubah flg asset
        $flgAsset = AssetsModel::find($asset_id);
        if ($flgAsset) {
            $flgAsset->update([
                'flg_status' => 'P'
            ]);
        }

        return redirect()->route('perbaikan-aktif')->with('success', 'Data berhasil disimpan.');
    }

    public function selesai_perbaikan(Request $request, $maintenance_active_id)
    {

        try {

            $validatedData = $request->validate([
                'f_breakdown_date' => 'required',
                'f_breakdown_time' => 'required',
                'finance'          => 'required',
            ]);

            $maintenanceActive = MaintenanceActiveModel::find($maintenance_active_id);
            $asset_id = $maintenanceActive->asset_id;

            if ($maintenanceActive) {
                $maintenanceActive->update([
                    'f_breakdown_date' => $validatedData['f_breakdown_date'],
                    'f_breakdown_time' => $validatedData['f_breakdown_time'],
                    'finance'          => $validatedData['finance']
                ]);

                $takeData = MaintenanceActiveModel::all()->map(function ($item) {
                    unset($item['maintenance_active_id']);
                    return $item;
                });

                $flgAsset = AssetsModel::find($asset_id);
                if ($flgAsset) {
                    $flgAsset->update([
                        'flg_status' => 'T'
                    ]);
                }

                foreach ($takeData as $data) {
                    MaintenanceHistoryModel::create($data->toArray());
                }
                $maintenanceActive->delete();
                return redirect()->route('perbaikan-aktif')->with('success', 'Proses perbaikan telah berhasil diselesaikan.');
            } else {
                return redirect()->route('perbaikan-aktif')->with('error', 'Proses perbaikan gagal diselesaikan.');
            }
        } catch (ValidationException $e) {
            return redirect()->route('perbaikan-aktif');
        }
    }

    public function edit_data($id, $asset_id)
    {
        $getAsset = AssetsModel::find($asset_id);
        $maintenanceActive = MaintenanceActiveModel::find($id);
        return view('pages.admin.perbaikan_aktif.perbaikan_ubah', compact('maintenanceActive', 'getAsset'));
    }

    public function update_data(Request $request, $id)
    {
        $validatedData = $request->validate([
            's_breakdown_date'  => 'required|date',
            's_breakdown_time'  => 'required',
            'asset_id'          => 'required|integer',
            'issue'             => 'required',
            'perform_by'        => 'required',
        ]);

        $user_id = Auth::user()->id_user;
        $maintenanceActive = MaintenanceActiveModel::find($id);
        $maintenanceActive->update([
            's_breakdown_date'  => $validatedData['s_breakdown_date'],
            's_breakdown_time'  => $validatedData['s_breakdown_time'],
            'asset_id'          => $validatedData['asset_id'],
            'issue'             => $validatedData['issue'],
            'perform_by'        => $validatedData['perform_by'],
            'record_adm_id'     => $user_id
        ]);



        return redirect()->route('perbaikan-aktif')->with('success', 'Data berhasil disimpan.');
    }
}
