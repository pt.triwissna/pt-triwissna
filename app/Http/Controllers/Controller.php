<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\RedirectResponse;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;
    public function loginShow()
    {
        return view('auth.login');
    }

    public function loginAction(Request $request): RedirectResponse
    {
        $credentials = $request->validate([
            'username' => ['required'],
            'password' => ['required'],
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->intended('/');
        }

        return back()->withErrors([
            'username' => 'Kombinasi username dan password tidak cocok.',
        ])->onlyInput('username');
    }


    public function profilView(){
        return view('auth.profil');
    }


    public function logout()
    {
        Auth::logout();
        return redirect()->route('login'); // atau halaman lain jika diperlukan setelah logout
    }
}