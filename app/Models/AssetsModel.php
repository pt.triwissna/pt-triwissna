<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AssetsModel extends Model
{
    protected $table = 'assets';
    protected $primaryKey = 'asset_id';

    protected $guarded = [];

    public function permits(){
        return $this->belongsTo(PermitsInsuranceModels::class, 'id_permits_insurance');
    }
    public function type(){
        return $this->belongsTo(TypeAssetModels::class, 'id_type');
    }
    public function ctgr(){
        return $this->belongsTo(CategoryAssetModels::class, 'id_ctgr');
    }

}
