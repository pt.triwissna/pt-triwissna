<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaintenanceActiveModel extends Model
{
    protected $table = 'maintenance_active';
    protected $primaryKey = 'maintenance_active_id';

    protected $guarded = [];
}
