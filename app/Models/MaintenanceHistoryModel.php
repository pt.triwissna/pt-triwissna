<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaintenanceHistoryModel extends Model
{
    protected $table = 'maintenance_history';
    protected $primaryKey = 'maintenance_id';

    protected $guarded = [];

    public function asset()
    {
        return $this->BelongsTo(AssetsModel::class, 'asset_id');
    }
}
