<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class OperationActive extends Model
{

    protected $table = 'operation_active';
    protected $primaryKey = 'id_operation_active';

    protected $guarded = [];

    public function asset()
    {
        return $this->hasOne(AssetsModel::class, 'asset_id', 'id_operation_active');
    }
    
}
