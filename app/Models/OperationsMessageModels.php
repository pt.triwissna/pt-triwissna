<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperationsMessageModels extends Model
{
    protected $table = 'operations_messages';
    protected $primaryKey = 'id_operations_message';

    protected $guarded = [];

    public function operations(){
        return $this->belongsTo(OperationActive::class, 'id_operation_active');
    }
}
