<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeAssetModels extends Model
{
    protected $table = 'type_asset';
    protected $primaryKey = 'id_type';

    protected $guarded = [];
    
}
