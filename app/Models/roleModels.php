<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class roleModels extends Model
{
    protected $table = 'role';
    protected $primaryKey = 'role_id';

    protected $guarded = [];
}
