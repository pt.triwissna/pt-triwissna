<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class OperationHistoryModels extends Model
{
    protected $table = 'operation_history';
    protected $primaryKey = 'id_operation';

    protected $guarded = [];

    public function asset()
    {
        return $this->BelongsTo(AssetsModel::class, 'asset_id');
    }
}
