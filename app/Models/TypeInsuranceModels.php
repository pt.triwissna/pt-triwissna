<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeInsuranceModels extends Model
{
    protected $table = 'type_insurance';
    protected $primaryKey = 'id_insurance_type';

    protected $guarded = [];
}
