<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryAssetModels extends Model
{
    protected $table = 'category_asset';
    protected $primaryKey = 'id_ctgr';

    protected $guarded = [];
}
