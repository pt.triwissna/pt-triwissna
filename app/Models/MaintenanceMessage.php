<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MaintenanceMessage extends Model
{
    protected $table = 'maintenance_messages';
    protected $primaryKey = 'id_maintenance_message';

    protected $guarded = [];
}
