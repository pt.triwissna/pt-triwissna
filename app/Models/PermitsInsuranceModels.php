<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermitsInsuranceModels extends Model
{
    use HasFactory;
    protected $table = 'permits_insurance';
    protected $primaryKey = 'id_permits_insurance';

    protected $guarded = [];


}
