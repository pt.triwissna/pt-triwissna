<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class dataUnitMessageModels extends Model
{
    protected $table = 'data_unit_messages';
    protected $primaryKey = 'id_dataUnit_message';

    protected $guarded = [];
}
